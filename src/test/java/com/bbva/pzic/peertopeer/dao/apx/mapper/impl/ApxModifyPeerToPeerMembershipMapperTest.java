package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembership;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxModifyPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.PeticionTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.RespuestaTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class ApxModifyPeerToPeerMembershipMapperTest {

    private IApxModifyPeerToPeerMembershipMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxModifyPeerToPeerMembershipMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputModifyPeerToPeerMembership input = EntityStubs.getInstance().getInputModifyPeerToPeerMembership();
        PeticionTransaccionPmndt201_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getMembershipId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertNotNull(result.getEntityin().getIspreferedreceiver());
        assertEquals(result.getEntityin().getIspreferedreceiver(), input.getMembership().getIsPreferedReceiver());
        assertNotNull(result.getEntityin().getStatus());
        assertNotNull(result.getEntityin().getStatus().getId());
        assertEquals(result.getEntityin().getStatus().getId(), input.getMembership().getStatus().getId());
    }

    @Test
    public void mapInWithoutStatusTest() throws IOException {
        InputModifyPeerToPeerMembership input = EntityStubs.getInstance().getInputModifyPeerToPeerMembership();
        input.getMembership().setStatus(null);
        PeticionTransaccionPmndt201_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getMembershipId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertNotNull(result.getEntityin().getIspreferedreceiver());
        assertEquals(result.getEntityin().getIspreferedreceiver(), input.getMembership().getIsPreferedReceiver());
        assertNull(result.getEntityin().getStatus());

    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPmndt201_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt201_1();
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertEquals(result.getStatus().getId(), input.getEntityout().getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertEquals(result.getStatus().getDescription(), input.getEntityout().getStatus().getDescription());
    }

    @Test
    public void mapOutWithoutStatusTest() throws IOException {
        RespuestaTransaccionPmndt201_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt201_1();
        input.getEntityout().setStatus(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
    }

    @Test
    public void mapOutWithoutEntityoutTest() {

        Membership result = mapper.mapOut(new RespuestaTransaccionPmndt201_1());

        assertNull(result);
    }

    @Test
    public void mapOutWithoutResponseNullTest() {
        Membership result = mapper.mapOut(null);

        assertNull(result);
    }

}
