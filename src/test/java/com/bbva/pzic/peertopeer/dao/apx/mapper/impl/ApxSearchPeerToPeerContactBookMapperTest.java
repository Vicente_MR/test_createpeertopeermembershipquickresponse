package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputSearchPeerToPeerContactBook;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxSearchPeerToPeerContactBookMapper;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.Data;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.Entityout;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.PeticionTransaccionPmndt401_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.RespuestaTransaccionPmndt401_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 2/26/2020.
 *
 * @author Cesardl
 */
public class ApxSearchPeerToPeerContactBookMapperTest {

    private IApxSearchPeerToPeerContactBookMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxSearchPeerToPeerContactBookMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputSearchPeerToPeerContactBook input = EntityStubs.getInstance().getInputSearchPeerToPeerContactBook();
        PeticionTransaccionPmndt401_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getContacts());
        assertEquals(result.getEntityin().getContacts().size(), input.getContactsBook().getContacts().size());
        assertNotNull(result.getEntityin().getContacts().get(0));
        assertNotNull(result.getEntityin().getContacts().get(0).getContact().getValue());
        assertEquals(result.getEntityin().getContacts().get(0).getContact().getValue(), input.getContactsBook().getContacts().get(0).getValue());
        assertNotNull(result.getEntityin().getContacts().get(0).getContact().getContacttype());
        assertNotNull(result.getEntityin().getContacts().get(0).getContact().getContacttype().getId());
        assertEquals(result.getEntityin().getContacts().get(0).getContact().getContacttype().getId(), input.getContactsBook().getContacts().get(0).getContactType().getId());
    }

    @Test
    public void mapInWithoutContacTypeTest() throws IOException {
        InputSearchPeerToPeerContactBook input = EntityStubs.getInstance().getInputSearchPeerToPeerContactBook();
        input.getContactsBook().getContacts().get(0).setContactType(null);
        PeticionTransaccionPmndt401_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getContacts());
        assertEquals(result.getEntityin().getContacts().size(), input.getContactsBook().getContacts().size());
        assertNotNull(result.getEntityin().getContacts().get(0));
        assertNotNull(result.getEntityin().getContacts().get(0).getContact().getValue());
        assertEquals(result.getEntityin().getContacts().get(0).getContact().getValue(), input.getContactsBook().getContacts().get(0).getValue());
        assertNull(result.getEntityin().getContacts().get(0).getContact().getContacttype());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPmndt401_1 responseBackend = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt401_1();
        ContactsBook result = mapper.mapOut(responseBackend);
        assertNotNull(result);
        assertNotNull(result.getContacts());
        assertEquals(result.getContacts().size(), responseBackend.getEntityout().getData().getContacts().size());
        assertNotNull(result.getContacts().get(0));
        assertNotNull(result.getContacts().get(0).getActive());
        assertEquals(result.getContacts().get(0).getActive(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getIsactive());
        assertNotNull(result.getContacts().get(0).getFirstName());
        assertEquals(result.getContacts().get(0).getFirstName(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getFirstname());
        assertNotNull(result.getContacts().get(0).getLastName());
        assertEquals(result.getContacts().get(0).getLastName(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getLastname());
        assertNotNull(result.getContacts().get(0).getValue());
        assertEquals(result.getContacts().get(0).getValue(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getValue());
        assertNotNull(result.getContacts().get(0).getContactType());
        assertNotNull(result.getContacts().get(0).getContactType().getId());
        assertEquals(result.getContacts().get(0).getContactType().getId(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getContacttype().getId());
    }

    @Test
    public void mapOutWithoutContactTypeTest() throws IOException {
        RespuestaTransaccionPmndt401_1 responseBackend = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt401_1();
        responseBackend.getEntityout().getData().getContacts().get(0).getContact().setContacttype(null);
        ContactsBook result = mapper.mapOut(responseBackend);
        assertNotNull(result);
        assertNotNull(result.getContacts());
        assertEquals(result.getContacts().size(), responseBackend.getEntityout().getData().getContacts().size());
        assertNotNull(result.getContacts().get(0));
        assertNotNull(result.getContacts().get(0).getActive());
        assertEquals(result.getContacts().get(0).getActive(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getIsactive());
        assertNotNull(result.getContacts().get(0).getFirstName());
        assertEquals(result.getContacts().get(0).getFirstName(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getFirstname());
        assertNotNull(result.getContacts().get(0).getLastName());
        assertEquals(result.getContacts().get(0).getLastName(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getLastname());
        assertNotNull(result.getContacts().get(0).getValue());
        assertEquals(result.getContacts().get(0).getValue(), responseBackend.getEntityout().getData().getContacts().get(0).getContact().getValue());
        assertNull(result.getContacts().get(0).getContactType());
    }

    @Test
    public void mapOutWithoutArrayContactsTest() {
        RespuestaTransaccionPmndt401_1 responseBackend = new RespuestaTransaccionPmndt401_1();
        responseBackend.setEntityout(new Entityout());
        responseBackend.getEntityout().setData(new Data());
        responseBackend.getEntityout().getData().setContacts(null);
        ContactsBook result = mapper.mapOut(responseBackend);
        assertNotNull(result);
        assertNull(result.getContacts());
    }

    @Test
    public void mapOutWithoutDataTest() {
        RespuestaTransaccionPmndt401_1 responseBackend = new RespuestaTransaccionPmndt401_1();
        responseBackend.setEntityout(new Entityout());
        responseBackend.getEntityout().setData(null);
        ContactsBook result = mapper.mapOut(responseBackend);
        assertNull(result);
    }

    @Test
    public void mapOutWithoutEntityoutTest() {
        RespuestaTransaccionPmndt401_1 responseBackend = new RespuestaTransaccionPmndt401_1();
        responseBackend.setEntityout(null);
        ContactsBook result = mapper.mapOut(responseBackend);
        assertNull(result);
    }

    @Test
    public void mapOutWithoutResponseNullTest() {
        ContactsBook result = mapper.mapOut(null);
        assertNull(result);
    }


}
