package com.bbva.pzic.peertopeer.dao.model.pmndt301_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PMNDT301</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPmndt301_1 {

    @InjectMocks
    private TransaccionPmndt301_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {

        PeticionTransaccionPmndt301_1 peticion = new PeticionTransaccionPmndt301_1();
        RespuestaTransaccionPmndt301_1 respuesta = new RespuestaTransaccionPmndt301_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPmndt301_1.class, RespuestaTransaccionPmndt301_1.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionPmndt301_1 result = transaccion.invocar(peticion);

        assertEquals(result, respuesta);
    }
}
