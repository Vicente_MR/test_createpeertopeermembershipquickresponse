package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxCreatePeerToPeerMembershipPaymentMethodMapper;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.PeticionTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.RespuestaTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class ApxCreatePeerToPeerMembershipPaymentMethodMapperTest {

    private IApxCreatePeerToPeerMembershipPaymentMethodMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxCreatePeerToPeerMembershipPaymentMethodMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputCreatePeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputCreatePeerToPeerMembershipPaymentMethod();
        PeticionTransaccionPmndt301_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityinput().getCustomerid());
        assertEquals(result.getEntityinput().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityinput().getMembershipId());
        assertEquals(result.getEntityinput().getMembershipId(), input.getMembershipId());

        assertNotNull(result.getEntityinput().getAlias());
        assertNotNull(result.getEntityinput().getAlias().getId());
        assertEquals(result.getEntityinput().getAlias().getId(), input.getPaymentMethod().getAlias().getId());
        assertNotNull(result.getEntityinput().getAlias().getValue());
        assertEquals(result.getEntityinput().getAlias().getValue(), input.getPaymentMethod().getAlias().getValue());
        assertNotNull(result.getEntityinput().getAlias().getAliastype());
        assertNotNull(result.getEntityinput().getAlias().getAliastype().getId());
        assertEquals(result.getEntityinput().getAlias().getAliastype().getId(), input.getPaymentMethod().getAlias().getAliasType().getId());

        assertNotNull(result.getEntityinput().getRelatedcontracts());
        assertEquals(result.getEntityinput().getRelatedcontracts().size(), input.getPaymentMethod().getRelatedContracts().size());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertEquals(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getContractid(),
                input.getPaymentMethod().getRelatedContracts().get(0).getContractId());

        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertEquals(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId(),
                input.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());

        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertEquals(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId(),
                input.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutProductTypeTest() throws IOException {
        InputCreatePeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputCreatePeerToPeerMembershipPaymentMethod();
        input.getPaymentMethod().getRelatedContracts().get(0).setProductType(null);
        PeticionTransaccionPmndt301_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityinput().getCustomerid());
        assertEquals(result.getEntityinput().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityinput().getMembershipId());
        assertEquals(result.getEntityinput().getMembershipId(), input.getMembershipId());

        assertNotNull(result.getEntityinput().getAlias());
        assertNotNull(result.getEntityinput().getAlias().getId());
        assertEquals(result.getEntityinput().getAlias().getId(), input.getPaymentMethod().getAlias().getId());
        assertNotNull(result.getEntityinput().getAlias().getValue());
        assertEquals(result.getEntityinput().getAlias().getValue(), input.getPaymentMethod().getAlias().getValue());
        assertNotNull(result.getEntityinput().getAlias().getAliastype());
        assertNotNull(result.getEntityinput().getAlias().getAliastype().getId());
        assertEquals(result.getEntityinput().getAlias().getAliastype().getId(), input.getPaymentMethod().getAlias().getAliasType().getId());

        assertNotNull(result.getEntityinput().getRelatedcontracts());
        assertEquals(result.getEntityinput().getRelatedcontracts().size(), input.getPaymentMethod().getRelatedContracts().size());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertEquals(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getContractid(),
                input.getPaymentMethod().getRelatedContracts().get(0).getContractId());

        assertNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype());

        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertEquals(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId(),
                input.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutRelationTypeTest() throws IOException {
        InputCreatePeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputCreatePeerToPeerMembershipPaymentMethod();
        input.getPaymentMethod().getRelatedContracts().get(0).setProductType(null);
        input.getPaymentMethod().getRelatedContracts().get(0).setRelationType(null);
        PeticionTransaccionPmndt301_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityinput().getCustomerid());
        assertEquals(result.getEntityinput().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityinput().getMembershipId());
        assertEquals(result.getEntityinput().getMembershipId(), input.getMembershipId());

        assertNotNull(result.getEntityinput().getAlias());
        assertNotNull(result.getEntityinput().getAlias().getId());
        assertEquals(result.getEntityinput().getAlias().getId(), input.getPaymentMethod().getAlias().getId());
        assertNotNull(result.getEntityinput().getAlias().getValue());
        assertEquals(result.getEntityinput().getAlias().getValue(), input.getPaymentMethod().getAlias().getValue());
        assertNotNull(result.getEntityinput().getAlias().getAliastype());
        assertNotNull(result.getEntityinput().getAlias().getAliastype().getId());
        assertEquals(result.getEntityinput().getAlias().getAliastype().getId(), input.getPaymentMethod().getAlias().getAliasType().getId());

        assertNotNull(result.getEntityinput().getRelatedcontracts());
        assertEquals(result.getEntityinput().getRelatedcontracts().size(), input.getPaymentMethod().getRelatedContracts().size());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract());
        assertNotNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertEquals(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getContractid(),
                input.getPaymentMethod().getRelatedContracts().get(0).getContractId());

        assertNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype());

        assertNull(result.getEntityinput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityoutput().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityoutput().getIsavailable());

        assertNotNull(result.getAlias());
        assertNotNull(result.getAlias().getId());
        assertEquals(result.getAlias().getId(), input.getEntityoutput().getAlias().getId());
        assertNotNull(result.getAlias().getValue());
        assertEquals(result.getAlias().getValue(), input.getEntityoutput().getAlias().getValue());
        assertNotNull(result.getAlias().getAliasType());
        assertNotNull(result.getAlias().getAliasType().getId());
        assertEquals(result.getAlias().getAliasType().getId(), input.getEntityoutput().getAlias().getAliastype().getId());
        assertNotNull(result.getAlias().getAliasType().getDescription());
        assertEquals(result.getAlias().getAliasType().getDescription(), input.getEntityoutput().getAlias().getAliastype().getDescription());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityoutput().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());
    }

    @Test
    public void mapOutWithoutAliasTypeTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        input.getEntityoutput().getAlias().setAliastype(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityoutput().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityoutput().getIsavailable());

        assertNotNull(result.getAlias());
        assertNotNull(result.getAlias().getId());
        assertEquals(result.getAlias().getId(), input.getEntityoutput().getAlias().getId());
        assertNotNull(result.getAlias().getValue());
        assertEquals(result.getAlias().getValue(), input.getEntityoutput().getAlias().getValue());
        assertNull(result.getAlias().getAliasType());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityoutput().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());
    }

    @Test
    public void mapOutWithoutAliasTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        input.getEntityoutput().setAlias(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityoutput().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityoutput().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityoutput().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());
    }

    @Test
    public void mapOutWithoutNumberTypeTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        input.getEntityoutput().setAlias(null);
        input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityoutput().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityoutput().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityoutput().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getRelatedContracts().get(0).getNumberType());

        assertNotNull(result.getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());
    }

    @Test
    public void mapOutWithoutProductTypeTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        input.getEntityoutput().setAlias(null);
        input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().setProducttype(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityoutput().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityoutput().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityoutput().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getRelatedContracts().get(0).getNumberType());

        assertNull(result.getRelatedContracts().get(0).getProductType());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());
    }

    @Test
    public void mapOutWithoutRelationTypeTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        input.getEntityoutput().setAlias(null);
        input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().setProducttype(null);
        input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().setRelationtype(null);

        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityoutput().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityoutput().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityoutput().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getRelatedContracts().get(0).getNumberType());
        assertNull(result.getRelatedContracts().get(0).getProductType());
        assertNull(result.getRelatedContracts().get(0).getRelationType());

    }

    @Test
    public void mapOutWithoutRelationTypeNameTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        input.getEntityoutput().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().setName(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityoutput().getRelatedcontracts().size());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNull(result.getRelatedContracts().get(0).getRelationType().getName());
    }

    @Test
    public void mapOutWithoutRelatedcontractTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        input.getEntityoutput().setAlias(null);
        input.getEntityoutput().getRelatedcontracts().get(0).setRelatedcontract(null);

        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityoutput().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityoutput().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityoutput().getRelatedcontracts().size());
        assertNull(result.getRelatedContracts().get(0));

    }

    @Test
    public void mapOutWithoutRelatedcontractsTest() throws IOException {
        RespuestaTransaccionPmndt301_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
        input.getEntityoutput().setAlias(null);
        input.getEntityoutput().setRelatedcontracts(null);

        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityoutput().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityoutput().getIsavailable());

        assertNull(result.getAlias());
        assertNull(result.getRelatedContracts());
    }

    @Test
    public void mapOutWithoutEntityoutTest() {
        PaymentMethod result = mapper.mapOut(new RespuestaTransaccionPmndt301_1());

        assertNull(result);
    }

    @Test
    public void mapOutWithoutResponseTest() {
        PaymentMethod result = mapper.mapOut(null);

        assertNull(result);
    }

}
