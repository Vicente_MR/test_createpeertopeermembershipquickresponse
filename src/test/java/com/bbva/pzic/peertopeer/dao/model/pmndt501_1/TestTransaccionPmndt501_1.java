package com.bbva.pzic.peertopeer.dao.model.pmndt501_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PMNDT501</code>
 * 
 * @author Arquitectura Spring BBVA
 */

@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPmndt501_1 {
		
	@InjectMocks
	private TransaccionPmndt501_1 transaccion;

	@Mock
	private ServicioTransacciones servicioTransacciones;
	
	@Test
	public void test()  {
		
		PeticionTransaccionPmndt501_1 peticion = new PeticionTransaccionPmndt501_1();
		RespuestaTransaccionPmndt501_1 respuesta = new RespuestaTransaccionPmndt501_1();

		when(servicioTransacciones.invocar(PeticionTransaccionPmndt501_1.class, RespuestaTransaccionPmndt501_1.class, peticion))
				.thenReturn(respuesta);

		RespuestaTransaccionPmndt501_1 result = transaccion.invocar(peticion);

		assertEquals(result, respuesta);

	}
}