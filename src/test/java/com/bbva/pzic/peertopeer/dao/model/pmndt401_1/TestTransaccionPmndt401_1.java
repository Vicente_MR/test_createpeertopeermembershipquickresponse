package com.bbva.pzic.peertopeer.dao.model.pmndt401_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PMNDT401</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPmndt401_1 {

    @InjectMocks
    private TransaccionPmndt401_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {

        PeticionTransaccionPmndt401_1 peticion = new PeticionTransaccionPmndt401_1();
        RespuestaTransaccionPmndt401_1 respuesta = new RespuestaTransaccionPmndt401_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPmndt401_1.class, RespuestaTransaccionPmndt401_1.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionPmndt401_1 result = transaccion.invocar(peticion);

        assertEquals(result, respuesta);
    }
}
