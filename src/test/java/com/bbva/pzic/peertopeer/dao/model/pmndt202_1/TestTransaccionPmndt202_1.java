package com.bbva.pzic.peertopeer.dao.model.pmndt202_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PMNDT202</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPmndt202_1 {

    @InjectMocks
    private TransaccionPmndt202_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;


    @Test
    public void test() {
        PeticionTransaccionPmndt202_1 peticion = new PeticionTransaccionPmndt202_1();
        RespuestaTransaccionPmndt202_1 respuesta = new RespuestaTransaccionPmndt202_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPmndt202_1.class, RespuestaTransaccionPmndt202_1.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionPmndt202_1 result = transaccion.invocar(peticion);

        assertEquals(result, respuesta);

    }
}
