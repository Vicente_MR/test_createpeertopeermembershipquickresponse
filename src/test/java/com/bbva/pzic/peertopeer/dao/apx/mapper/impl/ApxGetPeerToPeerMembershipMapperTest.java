package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxGetPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.Relatedcontracts;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.RespuestaTransaccionPmndt101_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class ApxGetPeerToPeerMembershipMapperTest {

    private IApxGetPeerToPeerMembershipMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxGetPeerToPeerMembershipMapper();
    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertEquals(result.getStatus().getId(), input.getEntityout().getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertEquals(result.getStatus().getDescription(), input.getEntityout().getStatus().getDescription());

        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNotNull(result.getPaymentMethods().get(0).getAlias());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getId());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getId());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getValue());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getValue(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getValue());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getAliasType());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getAliasType().getId());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getAliasType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getAliastype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getAliasType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getAliasType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getAliastype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().size(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().size());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());

    }

    @Test
    public void mapOutWithoutExpandTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setPaymentmethods(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertEquals(result.getStatus().getId(), input.getEntityout().getStatus().getId());
        assertNotNull(result.getStatus().getDescription());
        assertEquals(result.getStatus().getDescription(), input.getEntityout().getStatus().getDescription());

        assertNull(result.getPaymentMethods());
    }

    @Test
    public void mapOutWithoutStatusTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNotNull(result.getPaymentMethods().get(0).getAlias());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getId());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getId());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getValue());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getValue(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getValue());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getAliasType());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getAliasType().getId());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getAliasType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getAliastype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getAliasType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getAliasType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getAliastype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().size(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().size());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());

    }

    @Test
    public void mapOutWithoutAliasTypeTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().setAliastype(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNotNull(result.getPaymentMethods().get(0).getAlias());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getId());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getId());
        assertNotNull(result.getPaymentMethods().get(0).getAlias().getValue());
        assertEquals(result.getPaymentMethods().get(0).getAlias().getValue(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getAlias().getValue());
        assertNull(result.getPaymentMethods().get(0).getAlias().getAliasType());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().size(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().size());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());

    }

    @Test
    public void mapOutWithoutAliasTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().setAlias(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNull(result.getPaymentMethods().get(0).getAlias());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().size(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().size());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());

    }

    @Test
    public void mapOutWithoutNumberTypeTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().setAlias(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNull(result.getPaymentMethods().get(0).getAlias());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().size(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().size());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());

    }

    @Test
    public void mapOutWithoutProductTypeTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().setAlias(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().setProducttype(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNull(result.getPaymentMethods().get(0).getAlias());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().size(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().size());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType());

        assertNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());

    }

    @Test
    public void mapOutWithoutRelationTypeTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().setAlias(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().setProducttype(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().setRelationtype(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNull(result.getPaymentMethods().get(0).getAlias());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().size(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().size());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getContractId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber());
        assertEquals(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumber(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getNumberType());

        assertNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getProductType());

        assertNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0).getRelationType());
    }

    @Test
    public void mapOutWithoutPositionOneArrayRelatedContractsTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().setAlias(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().setRelatedcontracts(new ArrayList<>());
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().add(new Relatedcontracts());
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getRelatedcontracts().get(0).setRelatedcontract(null);
        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNull(result.getPaymentMethods().get(0).getAlias());

        assertNotNull(result.getPaymentMethods().get(0).getRelatedContracts());
        assertNull(result.getPaymentMethods().get(0).getRelatedContracts().get(0));
    }

    @Test
    public void mapOutWithoutRelatedContractsTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().setAlias(null);
        input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().setRelatedcontracts(null);

        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNotNull(result.getPaymentMethods().get(0));
        assertNotNull(result.getPaymentMethods().get(0).getId());
        assertEquals(result.getPaymentMethods().get(0).getId(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getId());
        assertNotNull(result.getPaymentMethods().get(0).getCreationDate());
        assertNotNull(result.getPaymentMethods().get(0).getLastUpdateDate());
        assertNotNull(result.getPaymentMethods().get(0).getAvailable());
        assertEquals(result.getPaymentMethods().get(0).getAvailable(), input.getEntityout().getPaymentmethods().get(0).getPaymentmethod().getIsavailable());

        assertNull(result.getPaymentMethods().get(0).getAlias());

        assertNull(result.getPaymentMethods().get(0).getRelatedContracts());
    }

    @Test
    public void mapOutWithoutInstancePositionOnePaymentMethodTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().getPaymentmethods().get(0).setPaymentmethod(null);

        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNotNull(result.getPaymentMethods());
        assertEquals(result.getPaymentMethods().size(), input.getEntityout().getPaymentmethods().size());
        assertNull(result.getPaymentMethods().get(0));

    }

    @Test
    public void mapOutWithoutInsPaymentMethodTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.getEntityout().setStatus(null);
        input.getEntityout().setPaymentmethods(null);

        Membership result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getIsPreferedReceiver());
        assertEquals(result.getIsPreferedReceiver(), input.getEntityout().getIspreferedreceiver());
        assertNotNull(result.getMembershipDate());
        assertNotNull(result.getLastUpdateDate());
        assertNull(result.getStatus());
        assertNull(result.getPaymentMethods());
    }

    @Test
    public void mapOutWithoutInsEntityoutTest() throws IOException {
        RespuestaTransaccionPmndt101_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
        input.setEntityout(null);

        Membership result = mapper.mapOut(input);

        assertNull(result);

    }

    @Test
    public void mapOutWithoutResponseTest() {
        Membership result = mapper.mapOut(null);

        assertNull(result);

    }

}
