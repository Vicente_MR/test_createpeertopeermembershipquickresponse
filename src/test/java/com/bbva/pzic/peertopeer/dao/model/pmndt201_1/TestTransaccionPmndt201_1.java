package com.bbva.pzic.peertopeer.dao.model.pmndt201_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PMNDT201</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPmndt201_1 {

    @InjectMocks
    private TransaccionPmndt201_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {

        PeticionTransaccionPmndt201_1 peticion = new PeticionTransaccionPmndt201_1();
        RespuestaTransaccionPmndt201_1 respuesta = new RespuestaTransaccionPmndt201_1();
        when(servicioTransacciones.invocar(PeticionTransaccionPmndt201_1.class, RespuestaTransaccionPmndt201_1.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionPmndt201_1 result = transaccion.invocar(peticion);

        assertEquals(result, respuesta);
    }
}
