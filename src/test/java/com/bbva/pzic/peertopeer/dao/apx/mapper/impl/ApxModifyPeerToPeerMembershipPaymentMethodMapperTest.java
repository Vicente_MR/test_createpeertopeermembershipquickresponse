package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.DTOIntPaymentMethod;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxModifyPeerToPeerMembershipPaymentMethodMapper;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.PeticionTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.RespuestaTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class ApxModifyPeerToPeerMembershipPaymentMethodMapperTest {

    private IApxModifyPeerToPeerMembershipPaymentMethodMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxModifyPeerToPeerMembershipPaymentMethodMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputModifyPeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputModifyPeerToPeerMembershipPaymentMethod();
        PeticionTransaccionPmndt202_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getMembershipId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertNotNull(result.getEntityin().getPaymentMethodId());
        assertEquals(result.getEntityin().getPaymentMethodId(), input.getPaymentMethodId());
        assertNotNull(result.getEntityin().getIsavailable());
        assertEquals(result.getEntityin().getIsavailable(), input.getPaymentMethod().getAvailable());

        assertNotNull(result.getEntityin().getAlias());
        assertNotNull(result.getEntityin().getAlias().getId());
        assertEquals(result.getEntityin().getAlias().getId(), input.getPaymentMethod().getAlias().getId());
        assertNotNull(result.getEntityin().getAlias().getValue());
        assertEquals(result.getEntityin().getAlias().getValue(), input.getPaymentMethod().getAlias().getValue());
        assertNotNull(result.getEntityin().getAlias().getAliastype());
        assertNotNull(result.getEntityin().getAlias().getAliastype().getId());
        assertEquals(result.getEntityin().getAlias().getAliastype().getId(), input.getPaymentMethod().getAlias().getAliasType().getId());

        assertNotNull(result.getEntityin().getRelatedcontracts());
        assertEquals(result.getEntityin().getRelatedcontracts().size(), input.getPaymentMethod().getRelatedContracts().size());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getContractid(),
                input.getPaymentMethod().getRelatedContracts().get(0).getContractId());

        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getProducttype());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId(),
                input.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());

        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId(),
                input.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutAliasTest() throws IOException {
        InputModifyPeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputModifyPeerToPeerMembershipPaymentMethod();
        input.getPaymentMethod().setAlias(null);
        PeticionTransaccionPmndt202_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getMembershipId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertNotNull(result.getEntityin().getPaymentMethodId());
        assertEquals(result.getEntityin().getPaymentMethodId(), input.getPaymentMethodId());
        assertNotNull(result.getEntityin().getIsavailable());
        assertEquals(result.getEntityin().getIsavailable(), input.getPaymentMethod().getAvailable());

        assertNull(result.getEntityin().getAlias());

        assertNotNull(result.getEntityin().getRelatedcontracts());
        assertEquals(result.getEntityin().getRelatedcontracts().size(), input.getPaymentMethod().getRelatedContracts().size());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getContractid(),
                input.getPaymentMethod().getRelatedContracts().get(0).getContractId());

        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getProducttype());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId(),
                input.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());

        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId(),
                input.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutProductTypeTest() throws IOException {
        InputModifyPeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputModifyPeerToPeerMembershipPaymentMethod();
        input.getPaymentMethod().setAlias(null);
        input.getPaymentMethod().getRelatedContracts().get(0).setProductType(null);
        PeticionTransaccionPmndt202_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getMembershipId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertNotNull(result.getEntityin().getPaymentMethodId());
        assertEquals(result.getEntityin().getPaymentMethodId(), input.getPaymentMethodId());
        assertNotNull(result.getEntityin().getIsavailable());
        assertEquals(result.getEntityin().getIsavailable(), input.getPaymentMethod().getAvailable());

        assertNull(result.getEntityin().getAlias());

        assertNotNull(result.getEntityin().getRelatedcontracts());
        assertEquals(result.getEntityin().getRelatedcontracts().size(), input.getPaymentMethod().getRelatedContracts().size());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getContractid(),
                input.getPaymentMethod().getRelatedContracts().get(0).getContractId());

        assertNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getProducttype());

        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId(),
                input.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutRelationTypeTest() throws IOException {
        InputModifyPeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputModifyPeerToPeerMembershipPaymentMethod();
        input.getPaymentMethod().setAlias(null);
        input.getPaymentMethod().getRelatedContracts().get(0).setProductType(null);
        input.getPaymentMethod().getRelatedContracts().get(0).setRelationType(null);
        PeticionTransaccionPmndt202_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getMembershipId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertNotNull(result.getEntityin().getPaymentMethodId());
        assertEquals(result.getEntityin().getPaymentMethodId(), input.getPaymentMethodId());
        assertNotNull(result.getEntityin().getIsavailable());
        assertEquals(result.getEntityin().getIsavailable(), input.getPaymentMethod().getAvailable());

        assertNull(result.getEntityin().getAlias());

        assertNotNull(result.getEntityin().getRelatedcontracts());
        assertEquals(result.getEntityin().getRelatedcontracts().size(), input.getPaymentMethod().getRelatedContracts().size());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract());
        assertNotNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertEquals(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getContractid(),
                input.getPaymentMethod().getRelatedContracts().get(0).getContractId());

        assertNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getProducttype());

        assertNull(result.getEntityin().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype());
    }

    @Test
    public void mapInWithoutRelatedContractsTest() throws IOException {
        InputModifyPeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputModifyPeerToPeerMembershipPaymentMethod();
        input.getPaymentMethod().setAlias(null);
        input.getPaymentMethod().setRelatedContracts(null);
        PeticionTransaccionPmndt202_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getMembershipId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertNotNull(result.getEntityin().getPaymentMethodId());
        assertEquals(result.getEntityin().getPaymentMethodId(), input.getPaymentMethodId());
        assertNotNull(result.getEntityin().getIsavailable());
        assertEquals(result.getEntityin().getIsavailable(), input.getPaymentMethod().getAvailable());

        assertNull(result.getEntityin().getAlias());

        assertNull(result.getEntityin().getRelatedcontracts());

    }

    @Test
    public void mapInInstancePaymentTest() throws IOException {
        InputModifyPeerToPeerMembershipPaymentMethod input = EntityStubs.getInstance().getInputModifyPeerToPeerMembershipPaymentMethod();
        input.setPaymentMethod(new DTOIntPaymentMethod());
        PeticionTransaccionPmndt202_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertNotNull(result.getEntityin().getMembershipId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertNotNull(result.getEntityin().getPaymentMethodId());
        assertEquals(result.getEntityin().getPaymentMethodId(), input.getPaymentMethodId());
        assertNull(result.getEntityin().getIsavailable());
        assertEquals(result.getEntityin().getIsavailable(), input.getPaymentMethod().getAvailable());

        assertNull(result.getEntityin().getAlias());

        assertNull(result.getEntityin().getRelatedcontracts());

    }

    @Test
    public void mapOutFullTest() throws IOException {
        RespuestaTransaccionPmndt202_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityout().getIsavailable());

        assertNotNull(result.getAlias());
        assertNotNull(result.getAlias().getId());
        assertEquals(result.getAlias().getId(), input.getEntityout().getAlias().getId());
        assertNotNull(result.getAlias().getValue());
        assertEquals(result.getAlias().getValue(), input.getEntityout().getAlias().getValue());
        assertNotNull(result.getAlias().getAliasType());
        assertNotNull(result.getAlias().getAliasType().getId());
        assertEquals(result.getAlias().getAliasType().getId(), input.getEntityout().getAlias().getAliastype().getId());
        assertNotNull(result.getAlias().getAliasType().getDescription());
        assertEquals(result.getAlias().getAliasType().getDescription(), input.getEntityout().getAlias().getAliastype().getDescription());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityout().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());


    }

    @Test
    public void mapOutWithoutAliasTypeTest() throws IOException {
        RespuestaTransaccionPmndt202_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
        input.getEntityout().getAlias().setAliastype(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityout().getIsavailable());

        assertNotNull(result.getAlias());
        assertNotNull(result.getAlias().getId());
        assertEquals(result.getAlias().getId(), input.getEntityout().getAlias().getId());
        assertNotNull(result.getAlias().getValue());
        assertEquals(result.getAlias().getValue(), input.getEntityout().getAlias().getValue());
        assertNull(result.getAlias().getAliasType());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityout().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());


    }

    @Test
    public void mapOutWithoutAliasTest() throws IOException {
        RespuestaTransaccionPmndt202_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
        input.getEntityout().setAlias(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityout().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityout().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNotNull(result.getRelatedContracts().get(0).getNumberType());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getId());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getNumberType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getNumberType().getDescription(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumbertype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());


    }

    @Test
    public void mapOutWithoutNumberTypeTest() throws IOException {
        RespuestaTransaccionPmndt202_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
        input.getEntityout().setAlias(null);
        input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityout().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityout().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getRelatedContracts().get(0).getNumberType());

        assertNotNull(result.getRelatedContracts().get(0).getProductType());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getId());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getProductType().getDescription());
        assertEquals(result.getRelatedContracts().get(0).getProductType().getDescription(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getProducttype().getDescription());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());


    }

    @Test
    public void mapOutWithoutProductTypeTest() throws IOException {
        RespuestaTransaccionPmndt202_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
        input.getEntityout().setAlias(null);
        input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().setProducttype(null);
        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityout().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityout().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getRelatedContracts().get(0).getNumberType());

        assertNull(result.getRelatedContracts().get(0).getProductType());

        assertNotNull(result.getRelatedContracts().get(0).getRelationType());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getId());
        assertNotNull(result.getRelatedContracts().get(0).getRelationType().getName());
        assertEquals(result.getRelatedContracts().get(0).getRelationType().getName(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getRelationtype().getName());
    }

    @Test
    public void mapOutWithoutRelationTypeTest() throws IOException {
        RespuestaTransaccionPmndt202_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
        input.getEntityout().setAlias(null);
        input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().setNumbertype(null);
        input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().setProducttype(null);
        input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().setRelationtype(null);

        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityout().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityout().getRelatedcontracts().size());
        assertNotNull(result.getRelatedContracts().get(0).getContractId());
        assertEquals(result.getRelatedContracts().get(0).getContractId(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getContractid());
        assertNotNull(result.getRelatedContracts().get(0).getNumber());
        assertEquals(result.getRelatedContracts().get(0).getNumber(), input.getEntityout().getRelatedcontracts().get(0).getRelatedcontract().getNumber());

        assertNull(result.getRelatedContracts().get(0).getNumberType());
        assertNull(result.getRelatedContracts().get(0).getProductType());
        assertNull(result.getRelatedContracts().get(0).getRelationType());

    }

    @Test
    public void mapOutWithoutRelatedcontractTest() throws IOException {
        RespuestaTransaccionPmndt202_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
        input.getEntityout().setAlias(null);
        input.getEntityout().getRelatedcontracts().get(0).setRelatedcontract(null);

        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityout().getIsavailable());

        assertNull(result.getAlias());

        assertNotNull(result.getRelatedContracts());
        assertEquals(result.getRelatedContracts().size(), input.getEntityout().getRelatedcontracts().size());
        assertNull(result.getRelatedContracts().get(0));

    }

    @Test
    public void mapOutWithoutRelatedcontractsTest() throws IOException {
        RespuestaTransaccionPmndt202_1 input = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
        input.getEntityout().setAlias(null);
        input.getEntityout().setRelatedcontracts(null);

        PaymentMethod result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), input.getEntityout().getId());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getLastUpdateDate());
        assertNotNull(result.getAvailable());
        assertEquals(result.getAvailable(), input.getEntityout().getIsavailable());

        assertNull(result.getAlias());

        assertNull(result.getRelatedContracts());
    }

    @Test
    public void mapOutWithoutEntityoutTest() {
        PaymentMethod result = mapper.mapOut(new RespuestaTransaccionPmndt202_1());

        assertNull(result);
    }

    @Test
    public void mapOutWithoutResponseTest() {
        PaymentMethod result = mapper.mapOut(null);

        assertNull(result);
    }

}
