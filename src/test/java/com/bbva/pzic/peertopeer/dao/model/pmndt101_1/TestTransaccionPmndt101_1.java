package com.bbva.pzic.peertopeer.dao.model.pmndt101_1;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test de la transacci&oacute;n <code>PMNDT101</code>
 *
 * @author Arquitectura Spring BBVA
 */
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionPmndt101_1 {

    @InjectMocks
    private TransaccionPmndt101_1 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() {
        PeticionTransaccionPmndt101_1 peticion = new PeticionTransaccionPmndt101_1();
        RespuestaTransaccionPmndt101_1 respuesta = new RespuestaTransaccionPmndt101_1();

        when(servicioTransacciones.invocar(PeticionTransaccionPmndt101_1.class, RespuestaTransaccionPmndt101_1.class, peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionPmndt101_1 result = transaccion.invocar(peticion);

        assertEquals(result, respuesta);
    }
}
