package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipQuickResponse;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxCreatePeerToPeerMembershipQuickResponseMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt501_1.PeticionTransaccionPmndt501_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 08/01/2021.
 *
 * @author Entelgy
 */
public class ApxCreatePeerToPeerMembershipQuickResponseMapperTest {

    private IApxCreatePeerToPeerMembershipQuickResponseMapper mapper;

    @Before
    public void setUp() {
        mapper = new ApxCreatePeerToPeerMembershipQuickResponseMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputCreatePeerToPeerMembershipQuickResponse input = EntityStubs.getInstance().getInputCreatePeerToPeerMembershipQuickResponse();
        PeticionTransaccionPmndt501_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertNotNull(result.getEntityin().getMembershipId());
        assertNotNull(result.getEntityin().getAlias());
        assertNotNull(result.getEntityin().getAlias().getId());
        assertNotNull(result.getEntityin().getAlias().getValue());
        assertNotNull(result.getEntityin().getAlias().getAliastype());
        assertNotNull(result.getEntityin().getAlias().getAliastype().getId());

        assertEquals(result.getEntityin().getCustomerid(), input.getCustomerId());
        assertEquals(result.getEntityin().getMembershipId(), input.getMembershipId());
        assertEquals(result.getEntityin().getAlias().getId(), input.getAlias().getId());
        assertEquals(result.getEntityin().getAlias().getValue(), input.getAlias().getValue());
        assertEquals(result.getEntityin().getAlias().getAliastype().getId(), input.getAlias().getAliasType().getId());
    }

    @Test
    public void mapInWithOutAliasIdAndAliasValueTest() throws IOException {
        InputCreatePeerToPeerMembershipQuickResponse input = EntityStubs.getInstance().getInputCreatePeerToPeerMembershipQuickResponse();
        input.getAlias().setId(null);
        input.getAlias().setValue(null);
        PeticionTransaccionPmndt501_1 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getEntityin().getCustomerid());
        assertNotNull(result.getEntityin().getMembershipId());
        assertNotNull(result.getEntityin().getAlias());
    }

    @Test
    public void mapOutNullTest() {
        QuickResponse result = mapper.mapOut(null);
        assertNull(result);
    }
}
