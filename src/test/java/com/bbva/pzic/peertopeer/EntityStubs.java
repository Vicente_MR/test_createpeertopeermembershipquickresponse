package com.bbva.pzic.peertopeer;

import com.bbva.pzic.peertopeer.business.dto.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import com.bbva.pzic.peertopeer.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * @author Entelgy
 */
public final class EntityStubs {

    public final static String MEMBERSHIP_ID = "M";
    public final static String PAYMENT_METHOD_ID = "Z";
    public final static String CUSTOMER_ID = "123456";
    public final static String EXPAND = "payment-methods";
    private static final EntityStubs INSTANCE = new EntityStubs();
    private ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private EntityStubs() {
    }

    public static EntityStubs getInstance() {
        return INSTANCE;
    }

    public Membership getMembership() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/membership.json"), Membership.class);
    }

    public PaymentMethod getPaymentMethod() throws IOException {
        PaymentMethod paymentMethod = objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/paymentMethod.json"), PaymentMethod.class);
        paymentMethod.setAvailable(true);
        return paymentMethod;
    }

    public PaymentMethod getPaymentMethodResponse() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/paymentMethodResponse.json"), PaymentMethod.class);
    }

    public ContactsBook getContactsBook() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/contactBooks.json"), ContactsBook.class);
    }

    public QuickResponse getQuickResponse() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/quickResponse.json"), QuickResponse.class);
    }

    public Membership getMembershipResponse() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/membershipDetailResponse.json"), Membership.class);
    }

    public Membership getModifyMembershipResponse() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/modifyMembershipResponse.json"), Membership.class);
    }

    public InputModifyPeerToPeerMembership getInputModifyPeerToPeerMembership() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputModifyPeerToPeerMembership.json"), InputModifyPeerToPeerMembership.class);
    }

    public InputModifyPeerToPeerMembershipPaymentMethod getInputModifyPeerToPeerMembershipPaymentMethod() throws IOException {
        InputModifyPeerToPeerMembershipPaymentMethod inputModifyPeerToPeerMembershipPaymentMethod = objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputModifyPeerToPeerMembershipPaymentMethod.json"), InputModifyPeerToPeerMembershipPaymentMethod.class);
        inputModifyPeerToPeerMembershipPaymentMethod.getPaymentMethod().setAvailable(true);
        return inputModifyPeerToPeerMembershipPaymentMethod;
    }

    public InputCreatePeerToPeerMembershipPaymentMethod getInputCreatePeerToPeerMembershipPaymentMethod() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputModifyPeerToPeerMembershipPaymentMethod.json"), InputCreatePeerToPeerMembershipPaymentMethod.class);
    }

    public InputSearchPeerToPeerContactBook getInputSearchPeerToPeerContactBook() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputSearchPeerToPeerContactBook.json"), InputSearchPeerToPeerContactBook.class);
    }

    public InputCreatePeerToPeerMembershipQuickResponse getInputCreatePeerToPeerMembershipQuickResponse() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputCreatePeerToPeerMembershipQuickResponse.json"), InputCreatePeerToPeerMembershipQuickResponse.class);
    }
}
