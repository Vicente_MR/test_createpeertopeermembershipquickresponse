package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.when;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ModifyPeerToPeerMembershipPaymentMethodMapperTest {

    @InjectMocks
    private ModifyPeerToPeerMembershipPaymentMethodMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    public void setUp() {
        when(serviceInvocationContext.getProperty("ASTAMxClientId")).thenReturn(EntityStubs.CUSTOMER_ID);
    }

    @Test
    public void mapInFullTest() throws IOException {
        setUp();
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID,
                        EntityStubs.PAYMENT_METHOD_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getId());
        Assert.assertEquals(input.getAlias().getId(), result.getPaymentMethod()
                .getAlias().getId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getValue());
        Assert.assertEquals(input.getAlias().getValue(), result
                .getPaymentMethod().getAlias().getValue());
        Assert.assertNotNull(result.getPaymentMethod().getAlias()
                .getAliasType());
        Assert.assertNotNull(result.getPaymentMethod().getAlias()
                .getAliasType().getId());
        Assert.assertEquals(input.getAlias().getAliasType().getId(), result
                .getPaymentMethod().getAlias().getAliasType().getId());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0)
                        .getContractId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProductType()
                .getId(), result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType().getId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getPaymentMethod()
                .getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertNotNull(result.getPaymentMethod()
                .getAvailable());
        Assert.assertEquals(input.getAvailable(), result.getPaymentMethod()
                .getAvailable());
    }

    @Test
    public void mapInWithoutAliasTypeTest() throws IOException {
        setUp();
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.getAlias().setAliasType(null);
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID,
                        EntityStubs.PAYMENT_METHOD_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getId());
        Assert.assertEquals(input.getAlias().getId(), result.getPaymentMethod()
                .getAlias().getId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getValue());
        Assert.assertEquals(input.getAlias().getValue(), result
                .getPaymentMethod().getAlias().getValue());
        Assert.assertNull(result.getPaymentMethod().getAlias()
                .getAliasType());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0)
                        .getContractId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProductType()
                .getId(), result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType().getId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getPaymentMethod()
                .getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertNotNull(result.getPaymentMethod()
                .getAvailable());
        Assert.assertEquals(input.getAvailable(), result.getPaymentMethod()
                .getAvailable());
    }

    @Test
    public void mapInWithoutAliasTest() throws IOException {
        setUp();
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.setAlias(null);
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID,
                        EntityStubs.PAYMENT_METHOD_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNull(result.getPaymentMethod().getAlias());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0)
                        .getContractId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProductType()
                .getId(), result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType().getId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getPaymentMethod()
                .getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertNotNull(result.getPaymentMethod()
                .getAvailable());
        Assert.assertEquals(input.getAvailable(), result.getPaymentMethod()
                .getAvailable());
    }

    @Test
    public void mapInWithoutAliasAndRelatedContractsProductTypeTest() throws IOException {
        setUp();
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.setAlias(null);
        input.getRelatedContracts().get(0).setProductType(null);
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID,
                        EntityStubs.PAYMENT_METHOD_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNull(result.getPaymentMethod().getAlias());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0)
                        .getContractId());
        Assert.assertNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getPaymentMethod()
                .getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertNotNull(result.getPaymentMethod()
                .getAvailable());
        Assert.assertEquals(input.getAvailable(), result.getPaymentMethod()
                .getAvailable());
    }

    @Test
    public void mapInWithoutAliasAndRelatedContractsProductTypeAndRelationTypeTest() throws IOException {
        setUp();
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.setAlias(null);
        input.getRelatedContracts().get(0).setProductType(null);
        input.getRelatedContracts().get(0).setRelationType(null);
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID,
                        EntityStubs.PAYMENT_METHOD_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNull(result.getPaymentMethod().getAlias());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0)
                        .getContractId());
        Assert.assertNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getProductType());
        Assert.assertNull(result.getPaymentMethod().getRelatedContracts()
                .get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod()
                .getAvailable());
        Assert.assertEquals(input.getAvailable(), result.getPaymentMethod()
                .getAvailable());
    }

    @Test
    public void mapInWithoutAliasAndRelatedContractsTest() throws IOException {
        setUp();
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.setAlias(null);
        input.setRelatedContracts(null);
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID,
                        EntityStubs.PAYMENT_METHOD_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNull(result.getPaymentMethod().getAlias());
        //array RelatedContracts
        Assert.assertNull(result.getPaymentMethod().getRelatedContracts());

        Assert.assertNotNull(result.getPaymentMethod()
                .getAvailable());
        Assert.assertEquals(input.getAvailable(), result.getPaymentMethod()
                .getAvailable());
    }

    @Test
    public void mapInWithoutBodyTest() {
        setUp();
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID,
                        EntityStubs.PAYMENT_METHOD_ID, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNull(result.getPaymentMethod());
    }

    @Test
    public void mapInWithoutCustomerIdTest() {
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID,
                        EntityStubs.PAYMENT_METHOD_ID, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNull(result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNull(result.getPaymentMethod());
    }

    @Test
    public void mapInEmptyTest() {
        setUp();
        InputModifyPeerToPeerMembershipPaymentMethod result = mapper.mapIn(
                EntityStubs.MEMBERSHIP_ID, EntityStubs.PAYMENT_METHOD_ID,
                new PaymentMethod());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethodId());
        Assert.assertEquals(EntityStubs.PAYMENT_METHOD_ID, result.getPaymentMethodId());
        Assert.assertNotNull(result.getPaymentMethod());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        PaymentMethod response = EntityStubs.getInstance().getPaymentMethodResponse();
        ServiceResponse<PaymentMethod> result = mapper
                .mapOut(response);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<PaymentMethod> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}
