package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.when;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreatePeerToPeerMembershipPaymentMethodMapperTest {

    @InjectMocks
    private CreatePeerToPeerMembershipPaymentMethodMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty("ASTAMxClientId")).thenReturn(EntityStubs.CUSTOMER_ID);
    }

    @Test
    public void mapInFullTest() throws IOException {
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        InputCreatePeerToPeerMembershipPaymentMethod result = mapper.mapIn(EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getId());
        Assert.assertEquals(input.getAlias().getId(), result.getPaymentMethod().getAlias().getId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getValue());
        Assert.assertEquals(input.getAlias().getValue(), result.getPaymentMethod().getAlias().getValue());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getAliasType());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getAliasType().getId());
        Assert.assertEquals(input.getAlias().getAliasType().getId(), result.getPaymentMethod().getAlias().getAliasType().getId());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(), result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProductType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithousAliasIdTest() throws IOException {
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.getAlias().setId(null);
        InputCreatePeerToPeerMembershipPaymentMethod result = mapper.mapIn(EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias());
        Assert.assertNull(result.getPaymentMethod().getAlias().getId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getValue());
        Assert.assertEquals(input.getAlias().getValue(), result.getPaymentMethod().getAlias().getValue());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getAliasType());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getAliasType().getId());
        Assert.assertEquals(input.getAlias().getAliasType().getId(), result.getPaymentMethod().getAlias().getAliasType().getId());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProductType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutAliasTypeTest() throws IOException {
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.getAlias().setAliasType(null);
        InputCreatePeerToPeerMembershipPaymentMethod result = mapper.mapIn(EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getId());
        Assert.assertEquals(input.getAlias().getId(), result.getPaymentMethod().getAlias().getId());
        Assert.assertNotNull(result.getPaymentMethod().getAlias().getValue());
        Assert.assertEquals(input.getAlias().getValue(), result.getPaymentMethod().getAlias().getValue());
        Assert.assertNull(result.getPaymentMethod().getAlias().getAliasType());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProductType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutAliasTest() throws IOException {
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.setAlias(null);
        InputCreatePeerToPeerMembershipPaymentMethod result = mapper.mapIn(EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNull(result.getPaymentMethod().getAlias());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProductType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getProductType().getId());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutProductTypeTest() throws IOException {
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.setAlias(null);
        input.getRelatedContracts().get(0).setProductType(null);
        InputCreatePeerToPeerMembershipPaymentMethod result = mapper.mapIn(EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNull(result.getPaymentMethod().getAlias());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getRelationType().getId());
    }

    @Test
    public void mapInWithoutProductTypeAndRelationTypeTest() throws IOException {
        PaymentMethod input = EntityStubs.getInstance().getPaymentMethod();
        input.setAlias(null);
        input.getRelatedContracts().get(0).setProductType(null);
        input.getRelatedContracts().get(0).setRelationType(null);
        InputCreatePeerToPeerMembershipPaymentMethod result = mapper.mapIn(EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNull(result.getPaymentMethod().getAlias());
        //array RelatedContracts
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts());
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0));
        Assert.assertNotNull(result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContractId(),
                result.getPaymentMethod().getRelatedContracts().get(0).getContractId());
        Assert.assertNull(result.getPaymentMethod().getRelatedContracts().get(0).getProductType());
        Assert.assertNull(result.getPaymentMethod().getRelatedContracts().get(0).getRelationType());
    }

    @Test
    public void mapInEmptyTest() {
        InputCreatePeerToPeerMembershipPaymentMethod result = mapper.mapIn(EntityStubs.MEMBERSHIP_ID, new PaymentMethod());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getPaymentMethod());
    }

    @Test
    public void mapInWithOutBodyTest() {
        InputCreatePeerToPeerMembershipPaymentMethod result = mapper.mapIn(EntityStubs.MEMBERSHIP_ID, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNull(result.getPaymentMethod());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        PaymentMethod response = EntityStubs.getInstance().getPaymentMethodResponse();
        ServiceResponse<PaymentMethod> result = mapper.mapOut(response);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<PaymentMethod> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}
