package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipQuickResponse;
import com.bbva.pzic.peertopeer.facade.v0.dto.Alias;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.when;

/**
 * Created on 08/01/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreatePeerToPeerMembershipQuickResponseMapperTest {

    @InjectMocks
    private CreatePeerToPeerMembershipQuickResponseMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty("ASTAMxClientId")).thenReturn(EntityStubs.CUSTOMER_ID);
    }

    @Test
    public void mapInFullTest() throws IOException {
        setUp();
        QuickResponse input = EntityStubs.getInstance().getQuickResponse();
        InputCreatePeerToPeerMembershipQuickResponse result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID, input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertNotNull(result.getAlias());
        Assert.assertNotNull(result.getAlias().getId());
        Assert.assertNotNull(result.getAlias().getValue());
        Assert.assertNotNull(result.getAlias().getAliasType());
        Assert.assertNotNull(result.getAlias().getAliasType().getId());

        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertEquals(input.getAlias().getId(), result.getAlias().getId());
        Assert.assertEquals(input.getAlias().getValue(), result.getAlias().getValue());
        Assert.assertEquals(input.getAlias().getAliasType().getId(), result.getAlias().getAliasType().getId());

    }

    @Test
    public void mapInWithOutAliasIdAndAliasValue() throws IOException{
        setUp();
        QuickResponse input = new QuickResponse();
        Alias alias = new Alias();
        alias.setId(null);
        alias.setValue(null);
        input.setAlias(alias);

        InputCreatePeerToPeerMembershipQuickResponse result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID, input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertNotNull(result.getAlias());
        Assert.assertNull(result.getAlias().getId());
        Assert.assertNull(result.getAlias().getValue());

        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());

    }

    @Test
    public void mapInWithOutBodyTest() throws  IOException {
        setUp();
        InputCreatePeerToPeerMembershipQuickResponse result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID, new QuickResponse());

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertNull(result.getAlias());

    }

    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<QuickResponse> result = mapper.mapOut(new QuickResponse());
        Assert.assertNotNull(result);
    }

    @Test
    public void mapOutNullTest() throws IOException {
        ServiceResponse<QuickResponse> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}
