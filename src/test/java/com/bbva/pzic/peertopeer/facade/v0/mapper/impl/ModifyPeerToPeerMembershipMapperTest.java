package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembership;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.when;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ModifyPeerToPeerMembershipMapperTest {

    @InjectMocks
    private ModifyPeerToPeerMembershipMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty("ASTAMxClientId")).thenReturn(EntityStubs.CUSTOMER_ID);
    }

    @Test
    public void mapInFullTest() throws IOException {
        Membership input = EntityStubs.getInstance().getMembership();
        InputModifyPeerToPeerMembership result = mapper.mapIn(
                EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getMembership().getIsPreferedReceiver());
        Assert.assertEquals(input.getIsPreferedReceiver(), result
                .getMembership().getIsPreferedReceiver());
        Assert.assertNotNull(result.getMembership().getStatus());
        Assert.assertNotNull(result.getMembership().getStatus().getId());
        Assert.assertEquals(input.getStatus().getId(), result.getMembership()
                .getStatus().getId());
    }

    @Test
    public void mapInWithoutStatusTest() throws IOException {
        Membership input = EntityStubs.getInstance().getMembership();
        input.setStatus(null);
        InputModifyPeerToPeerMembership result = mapper.mapIn(
                EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getMembership().getIsPreferedReceiver());
        Assert.assertEquals(input.getIsPreferedReceiver(), result
                .getMembership().getIsPreferedReceiver());
        Assert.assertNull(result.getMembership().getStatus());
    }

    @Test
    public void mapInWithoutStatusAndIsPreferedReceiverTest() throws IOException {
        Membership input = EntityStubs.getInstance().getMembership();
        input.setStatus(null);
        input.setIsPreferedReceiver(null);
        InputModifyPeerToPeerMembership result = mapper.mapIn(
                EntityStubs.MEMBERSHIP_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getMembership());
    }

    @Test
    public void mapInEmptyTest() {
        InputModifyPeerToPeerMembership result = mapper.mapIn(
                EntityStubs.MEMBERSHIP_ID, new Membership());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
    }

    @Test
    public void mapInWithoutBodyTest() {
        InputModifyPeerToPeerMembership result = mapper.mapIn(
                EntityStubs.MEMBERSHIP_ID, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNull(result.getMembership());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<Membership> result = mapper.mapOut(new Membership());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<Membership> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}
