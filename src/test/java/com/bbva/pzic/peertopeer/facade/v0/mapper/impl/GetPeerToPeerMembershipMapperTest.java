package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputGetPeerToPeerMembership;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.when;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class GetPeerToPeerMembershipMapperTest {

    @InjectMocks
    private GetPeerToPeerMembershipMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty("ASTAMxClientId")).thenReturn(EntityStubs.CUSTOMER_ID);
    }

    @Test
    public void mapInFullTest() {
        InputGetPeerToPeerMembership result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID, EntityStubs.EXPAND);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getExpand());
        Assert.assertEquals(EntityStubs.EXPAND, result.getExpand());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());

    }

    @Test
    public void mapInWithoutExpandTest() {
        InputGetPeerToPeerMembership result = mapper
                .mapIn(EntityStubs.MEMBERSHIP_ID, null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getMembershipId());
        Assert.assertEquals(EntityStubs.MEMBERSHIP_ID, result.getMembershipId());
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNull(result.getExpand());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        Membership membership = EntityStubs.getInstance().getMembershipResponse();
        ServiceResponse<Membership> result = mapper.mapOut(membership);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<Membership> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}
