package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.EntityStubs;
import com.bbva.pzic.peertopeer.business.dto.InputSearchPeerToPeerContactBook;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;

import static org.mockito.Mockito.when;

/**
 * Created on 2/26/2020.
 *
 * @author Cesardl
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchPeerToPeerContactBookMapperTest {

    @InjectMocks
    private SearchPeerToPeerContactBookMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty("ASTAMxClientId")).thenReturn(EntityStubs.CUSTOMER_ID);
    }

    @Test
    public void mapInFullTest() throws IOException {
        ContactsBook input = EntityStubs.getInstance().getContactsBook();
        InputSearchPeerToPeerContactBook result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getContactsBook());
        Assert.assertNotNull(result.getContactsBook().getContacts());
        Assert.assertEquals(result.getContactsBook().getContacts().size(), input.getContacts().size());
        Assert.assertNotNull(result.getContactsBook().getContacts().get(0));
        Assert.assertNotNull(result.getContactsBook().getContacts().get(0).getValue());
        Assert.assertEquals(result.getContactsBook().getContacts().get(0).getValue(), input.getContacts().get(0).getValue());
        Assert.assertNotNull(result.getContactsBook().getContacts().get(0).getContactType());
        Assert.assertNotNull(result.getContactsBook().getContacts().get(0).getContactType().getId());
        Assert.assertEquals(result.getContactsBook().getContacts().get(0).getContactType().getId(), input.getContacts().get(0).getContactType().getId());
    }

    @Test
    public void mapInContacTypeTest() throws IOException {
        ContactsBook input = EntityStubs.getInstance().getContactsBook();
        input.getContacts().get(0).setContactType(null);
        InputSearchPeerToPeerContactBook result = mapper.mapIn(input);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getContactsBook());
        Assert.assertNotNull(result.getContactsBook().getContacts());
        Assert.assertEquals(result.getContactsBook().getContacts().size(), input.getContacts().size());
        Assert.assertNotNull(result.getContactsBook().getContacts().get(0));
        Assert.assertNotNull(result.getContactsBook().getContacts().get(0).getValue());
        Assert.assertEquals(result.getContactsBook().getContacts().get(0).getValue(), input.getContacts().get(0).getValue());
        Assert.assertNull(result.getContactsBook().getContacts().get(0).getContactType());
    }

    @Test
    public void mapInWithOutContactBooksTest() {

        InputSearchPeerToPeerContactBook result = mapper.mapIn(null);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNull(result.getContactsBook());
    }

    @Test
    public void mapInWithOutArrayContactTest() {
        ContactsBook contactsBook = new ContactsBook();
        contactsBook.setContacts(new ArrayList<>());
        InputSearchPeerToPeerContactBook result = mapper.mapIn(contactsBook);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCustomerId());
        Assert.assertEquals(EntityStubs.CUSTOMER_ID, result.getCustomerId());
        Assert.assertNotNull(result.getContactsBook());
        Assert.assertNull(result.getContactsBook().getContacts());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<ContactsBook> result = mapper.mapOut(new ContactsBook());
        Assert.assertNotNull(result);
    }

    @Test
    public void mapOutResponseNullTest() {
        ServiceResponse<ContactsBook> result = mapper.mapOut(null);
        Assert.assertNull(result);
    }

}
