package com.bbva.pzic.peertopeer.dao.apx.mapper;

import com.bbva.pzic.peertopeer.business.dto.InputSearchPeerToPeerContactBook;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.PeticionTransaccionPmndt401_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.RespuestaTransaccionPmndt401_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;

/**
 * Created on 2/26/2020.
 *
 * @author Cesardl
 */
public interface IApxSearchPeerToPeerContactBookMapper {
    PeticionTransaccionPmndt401_1 mapIn(InputSearchPeerToPeerContactBook input);

    ContactsBook mapOut(RespuestaTransaccionPmndt401_1 responseBackend);
}
