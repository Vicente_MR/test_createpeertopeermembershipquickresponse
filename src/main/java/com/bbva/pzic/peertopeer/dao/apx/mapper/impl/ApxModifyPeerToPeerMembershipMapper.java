package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.business.dto.DTOIntStatus;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembership;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxModifyPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.Entityin;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.PeticionTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.RespuestaTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.Status;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import com.bbva.pzic.peertopeer.util.mappers.Mapper;

import com.bbva.pzic.routine.commons.utils.DateUtils;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ApxModifyPeerToPeerMembershipMapper implements IApxModifyPeerToPeerMembershipMapper {
    @Override
    public PeticionTransaccionPmndt201_1 mapIn(InputModifyPeerToPeerMembership input) {
        PeticionTransaccionPmndt201_1 request = new PeticionTransaccionPmndt201_1();
        request.setEntityin(new Entityin());
        request.getEntityin().setCustomerid(input.getCustomerId());
        request.getEntityin().setMembershipId(input.getMembershipId());
        request.getEntityin().setIspreferedreceiver(input.getMembership().getIsPreferedReceiver());
        request.getEntityin().setStatus(getStatus(input.getMembership().getStatus()));
        return request;
    }

    @Override
    public Membership mapOut(RespuestaTransaccionPmndt201_1 responseBackend) {
        if (responseBackend == null || responseBackend.getEntityout() == null) {
            return null;
        }
        Membership response = new Membership();
        response.setId(responseBackend.getEntityout().getId());
        response.setIsPreferedReceiver(responseBackend.getEntityout().getIspreferedreceiver());
        response.setMembershipDate(DateUtils.rebuildDateTime(responseBackend.getEntityout().getMembershipdate()));
        response.setLastUpdateDate(DateUtils.rebuildDateTime(responseBackend.getEntityout().getLastupdatedate()));
        response.setStatus(getStatusModel(responseBackend.getEntityout().getStatus()));
        return response;
    }

    private com.bbva.pzic.peertopeer.facade.v0.dto.Status getStatusModel(Status input) {
        if (input == null) {
            return null;
        }
        com.bbva.pzic.peertopeer.facade.v0.dto.Status status = new com.bbva.pzic.peertopeer.facade.v0.dto.Status();
        status.setId(input.getId());
        status.setDescription(input.getDescription());
        return status;
    }

    private Status getStatus(final DTOIntStatus input) {
        if (input == null) {
            return null;
        }
        Status status = new Status();
        status.setId(input.getId());
        return status;
    }
}
