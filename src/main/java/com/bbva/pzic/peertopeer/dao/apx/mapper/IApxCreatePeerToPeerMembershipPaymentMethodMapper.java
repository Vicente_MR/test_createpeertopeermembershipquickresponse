package com.bbva.pzic.peertopeer.dao.apx.mapper;

import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.PeticionTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.RespuestaTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface IApxCreatePeerToPeerMembershipPaymentMethodMapper {
    PeticionTransaccionPmndt301_1 mapIn(InputCreatePeerToPeerMembershipPaymentMethod input);

    PaymentMethod mapOut(RespuestaTransaccionPmndt301_1 responseBackend);
}
