package com.bbva.pzic.peertopeer.dao.model.pmndt201_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PMNDT201</code>
 * 
 * @see PeticionTransaccionPmndt201_1
 * @see RespuestaTransaccionPmndt201_1
 */
@Component
public class TransaccionPmndt201_1 implements InvocadorTransaccion<PeticionTransaccionPmndt201_1,RespuestaTransaccionPmndt201_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPmndt201_1 invocar(PeticionTransaccionPmndt201_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt201_1.class, RespuestaTransaccionPmndt201_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPmndt201_1 invocarCache(PeticionTransaccionPmndt201_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt201_1.class, RespuestaTransaccionPmndt201_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}