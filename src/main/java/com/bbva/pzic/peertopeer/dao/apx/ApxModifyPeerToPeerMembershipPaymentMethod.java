package com.bbva.pzic.peertopeer.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxModifyPeerToPeerMembershipPaymentMethodMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.PeticionTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.RespuestaTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Component("apxModifyPeerToPeerMembershipPaymentMethod")
public class ApxModifyPeerToPeerMembershipPaymentMethod {
    private static final Log LOG = LogFactory.getLog(ApxModifyPeerToPeerMembershipPaymentMethod.class);

    @Resource(name = "apxModifyPeerToPeerMembershipPaymentMethodMapper")
    private IApxModifyPeerToPeerMembershipPaymentMethodMapper mapper;

    @Autowired
    private transient InvocadorTransaccion<PeticionTransaccionPmndt202_1, RespuestaTransaccionPmndt202_1> transaccion;

    public PaymentMethod invoke(final InputModifyPeerToPeerMembershipPaymentMethod input) {
        LOG.info("... Invoking method ApxGetPeerToPeerMembership.invoke...");
        PeticionTransaccionPmndt202_1 peticion = mapper.mapIn(input);
        RespuestaTransaccionPmndt202_1 respuesta = transaccion.invocar(peticion);
        return mapper.mapOut(respuesta);
    }
}
