package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.business.dto.DTOIntAlias;
import com.bbva.pzic.peertopeer.business.dto.DTOIntAliasType;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipQuickResponse;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxCreatePeerToPeerMembershipQuickResponseMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt501_1.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.AliasType;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 01/07/2020.
 *
 * @author Entelgy
 */
@Component
public class ApxCreatePeerToPeerMembershipQuickResponseMapper implements IApxCreatePeerToPeerMembershipQuickResponseMapper {

    private static final Log LOG = LogFactory.getLog(ApxCreatePeerToPeerMembershipQuickResponseMapper.class);
    @Override
    public PeticionTransaccionPmndt501_1 mapIn(final InputCreatePeerToPeerMembershipQuickResponse input) {
        LOG.info("... called method ApxCreatePeerToPeerMembershipQuickResponseMapper.mapIn ...");
        PeticionTransaccionPmndt501_1 peticion = new PeticionTransaccionPmndt501_1();
        peticion.setEntityin(new Entityin());
        peticion.getEntityin().setCustomerid(input.getCustomerId());
        peticion.getEntityin().setMembershipId(input.getMembershipId());
        peticion.getEntityin().setAlias(getAlias(input.getAlias()));
        return peticion;
    }

    private Alias getAlias(final DTOIntAlias alias) {
        if(alias == null){
            return null;
        }
        Alias request = new Alias();
        request.setId(alias.getId());
        request.setValue(alias.getValue());
        request.setAliastype(getAliasType(alias.getAliasType()));
        return request;
    }

    private Aliastype getAliasType(final DTOIntAliasType aliasType) {
        if(aliasType == null) {
            return null;
        }
        Aliastype request = new Aliastype();
        request.setId(aliasType.getId());
        return request;
    }

    @Override
    public QuickResponse mapOut(final RespuestaTransaccionPmndt501_1 mapOut) {
        LOG.info("... called method ApxCreatePeerToPeerMembershipQuickResponseMapper.mapOut ...");
        if(mapOut == null || mapOut.getEntityout() == null){
            return null;
        }

        QuickResponse quickResponse = new QuickResponse();
        quickResponse.setCustomerId(mapOut.getEntityout().getId());
        quickResponse.setAlias(getAliasResponse(mapOut.getEntityout().getAlias()));
        quickResponse.setLinks(getLinksResponse(mapOut.getEntityout().getLinks()));
        quickResponse.setCreationDate(DateUtils.rebuildDateTime(mapOut.getEntityout().getCreationdate()));
        return quickResponse;
    }

    private List<com.bbva.pzic.peertopeer.facade.v0.dto.Links> getLinksResponse(final List<Links> links) {
        if(CollectionUtils.isEmpty(links)){
            return null;
        }

        return links.stream().filter(Objects::nonNull).map(this::getLinks).collect(Collectors.toList());
    }

    private com.bbva.pzic.peertopeer.facade.v0.dto.Links getLinks(final Links links) {
        com.bbva.pzic.peertopeer.facade.v0.dto.Links response = new com.bbva.pzic.peertopeer.facade.v0.dto.Links();
        response.setHref(links.getLink().getHref());
        return response;
    }

    private com.bbva.pzic.peertopeer.facade.v0.dto.Alias getAliasResponse(final Alias alias) {
        if(alias == null){
            return null;
        }
        com.bbva.pzic.peertopeer.facade.v0.dto.Alias response = new com.bbva.pzic.peertopeer.facade.v0.dto.Alias();
        response.setId(alias.getId());
        response.setValue(alias.getValue());
        response.setAliasType(getAliasTypeResponse(alias.getAliastype()));
        return response;
    }

    private AliasType getAliasTypeResponse(final Aliastype aliastype) {
        if (aliastype == null) {
            return null;
        }
        AliasType response = new AliasType();
        response.setId(aliastype.getId());
        response.setDescription(aliastype.getDescription());
        return response;
    }
}
