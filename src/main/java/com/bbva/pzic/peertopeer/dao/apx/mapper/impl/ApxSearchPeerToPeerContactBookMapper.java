package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.business.dto.DTOIntContact;
import com.bbva.pzic.peertopeer.business.dto.DTOIntContactType;
import com.bbva.pzic.peertopeer.business.dto.InputSearchPeerToPeerContactBook;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxSearchPeerToPeerContactBookMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactType;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import com.bbva.pzic.peertopeer.facade.v0.mapper.impl.SearchPeerToPeerContactBookMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 2/26/2020.
 *
 * @author Cesardl
 */
@Component
public class ApxSearchPeerToPeerContactBookMapper implements IApxSearchPeerToPeerContactBookMapper {

    private static final Log LOG = LogFactory.getLog(ApxSearchPeerToPeerContactBookMapper.class);

    @Override
    public PeticionTransaccionPmndt401_1 mapIn(final InputSearchPeerToPeerContactBook input) {
        LOG.info("... called method ApxSearchPeerToPeerContactBookMapper.mapIn ...");
        PeticionTransaccionPmndt401_1 request = new PeticionTransaccionPmndt401_1();
        request.setEntityin(new Entityin());
        request.getEntityin().setCustomerid(input.getCustomerId());
        request.getEntityin().setContacts(getContactsListModelBackend(input.getContactsBook().getContacts()));
        return request;
    }

    private List<Contacts> getContactsListModelBackend(final List<DTOIntContact> dtoIntContacts) {
        return dtoIntContacts.stream().filter(Objects::nonNull).map(this::getContactsModelBackend).collect(Collectors.toList());
    }

    private Contacts getContactsModelBackend(final DTOIntContact dtoIntContact) {
        Contacts contacts = new Contacts();
        contacts.setContact(new Contact());
        contacts.getContact().setValue(dtoIntContact.getValue());
        contacts.getContact().setContacttype(getContacttypeModelBackend(dtoIntContact.getContactType()));
        return contacts;
    }

    private Contacttype getContacttypeModelBackend(final DTOIntContactType dtoIntContactType) {
        if (dtoIntContactType == null) {
            return null;
        }
        Contacttype contacttype = new Contacttype();
        contacttype.setId(dtoIntContactType.getId());
        return contacttype;
    }

    @Override
    public ContactsBook mapOut(final RespuestaTransaccionPmndt401_1 responseBackend) {
        LOG.info("... called method ApxSearchPeerToPeerContactBookMapper.mapOut ...");
        if (responseBackend == null || responseBackend.getEntityout() == null || responseBackend.getEntityout().getData() == null) {
            return null;
        }
        ContactsBook response = new ContactsBook();
        response.setContacts(getContactListModel(responseBackend.getEntityout().getData().getContacts()));
        return response;
    }

    private List<com.bbva.pzic.peertopeer.facade.v0.dto.Contact> getContactListModel(final List<Contacts> contacts) {
        if (CollectionUtils.isEmpty(contacts)) {
            return null;
        }
        return contacts.stream().filter(Objects::nonNull).map(this::getContactModel).collect(Collectors.toList());
    }

    private com.bbva.pzic.peertopeer.facade.v0.dto.Contact getContactModel(final Contacts contacts) {
        com.bbva.pzic.peertopeer.facade.v0.dto.Contact response = new com.bbva.pzic.peertopeer.facade.v0.dto.Contact();
        response.setFirstName(contacts.getContact().getFirstname());
        response.setLastName(contacts.getContact().getLastname());
        response.setValue(contacts.getContact().getValue());
        response.setActive(contacts.getContact().getIsactive());
        response.setContactType(getContactTypeModel(contacts.getContact().getContacttype()));
        return response;
    }

    private ContactType getContactTypeModel(final Contacttype contacttype) {
        if (contacttype == null) {
            return null;
        }
        ContactType contactType = new ContactType();
        contactType.setId(contacttype.getId());
        return contactType;
    }
}
