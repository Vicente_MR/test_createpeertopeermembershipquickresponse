package com.bbva.pzic.peertopeer.dao.model.pmndt201_1.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.PeticionTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.RespuestaTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TransaccionPmndt201_1Mock implements InvocadorTransaccion<PeticionTransaccionPmndt201_1, RespuestaTransaccionPmndt201_1> {
    public static final String TEST_EMPTY = "999";
    public static final String TEST_RESPONSE_NULL = "777";
    @Override
    public RespuestaTransaccionPmndt201_1 invocar(PeticionTransaccionPmndt201_1 request) throws ExcepcionTransaccion {
        try {
            RespuestaTransaccionPmndt201_1 respuesta = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt201_1();
            if (request.getEntityin().getMembershipId().equalsIgnoreCase(TEST_EMPTY)) {
                return new RespuestaTransaccionPmndt201_1();
            }else if (request.getEntityin().getMembershipId().equalsIgnoreCase(TEST_RESPONSE_NULL)){
                return null;
            }
            return respuesta;

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPmndt201_1 invocarCache(PeticionTransaccionPmndt201_1 peticionTransaccionPmndt201_1) {
        return null;
    }

    @Override
    public void vaciarCache() {

    }
}
