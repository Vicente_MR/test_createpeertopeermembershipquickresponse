package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.business.dto.*;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxModifyPeerToPeerMembershipPaymentMethodMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.Alias;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.*;
import com.bbva.pzic.peertopeer.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ApxModifyPeerToPeerMembershipPaymentMethodMapper implements IApxModifyPeerToPeerMembershipPaymentMethodMapper {

    @Override
    public PeticionTransaccionPmndt202_1 mapIn(final InputModifyPeerToPeerMembershipPaymentMethod input) {
        PeticionTransaccionPmndt202_1 request = new PeticionTransaccionPmndt202_1();
        request.setEntityin(new Entityin());
        request.getEntityin().setCustomerid(input.getCustomerId());
        request.getEntityin().setMembershipId(input.getMembershipId());
        request.getEntityin().setPaymentMethodId(input.getPaymentMethodId());
        request.getEntityin().setIsavailable(input.getPaymentMethod().getAvailable());
        //alias
        request.getEntityin().setAlias(getAliasModelBackend(input.getPaymentMethod().getAlias()));
        //relatedContracts
        request.getEntityin().setRelatedcontracts(getRelatedcontractsListModelBackend(input.getPaymentMethod().getRelatedContracts()));
        return request;
    }

    private List<Relatedcontracts> getRelatedcontractsListModelBackend(final List<DTOIntRelatedContract> relatedContractList) {
        if (CollectionUtils.isEmpty(relatedContractList)) {
            return null;
        }
        return relatedContractList.stream().filter(Objects::nonNull).map(this::getRelatedcontractsModelBackend).collect(Collectors.toList());
    }

    private Relatedcontracts getRelatedcontractsModelBackend(final DTOIntRelatedContract relatedContract) {
        Relatedcontracts request = new Relatedcontracts();
        request.setRelatedcontract(new Relatedcontract());
        request.getRelatedcontract().setContractid(relatedContract.getContractId());
        request.getRelatedcontract().setProducttype(getProducttypeModelBackend(relatedContract.getProductType()));
        request.getRelatedcontract().setRelationtype(getRelationtypeModelBackend(relatedContract.getRelationType()));
        return request;
    }

    private Relationtype getRelationtypeModelBackend(final DTOIntRelationType relationType) {
        if (relationType == null) {
            return null;
        }
        Relationtype request = new Relationtype();
        request.setId(relationType.getId());
        return request;
    }

    private Producttype getProducttypeModelBackend(final DTOIntProductType productType) {
        if (productType == null) {
            return null;
        }
        Producttype request = new Producttype();
        request.setId(productType.getId());
        return request;
    }

    private Alias getAliasModelBackend(final DTOIntAlias dtoInt) {
        if (dtoInt == null) {
            return null;
        }
        Alias alias = new Alias();
        alias.setId(dtoInt.getId());
        alias.setValue(dtoInt.getValue());
        //aliasType
        alias.setAliastype(getAliasTypeModelBackend(dtoInt.getAliasType()));
        return alias;
    }

    private Aliastype getAliasTypeModelBackend(final DTOIntAliasType input) {
        Aliastype aliastype = new Aliastype();
        aliastype.setId(input.getId());
        return aliastype;
    }

    @Override
    public PaymentMethod mapOut(final RespuestaTransaccionPmndt202_1 responseBackend) {
        if (responseBackend == null || responseBackend.getEntityout() == null) {
            return null;
        }
        PaymentMethod response = new PaymentMethod();
        response.setId(responseBackend.getEntityout().getId());
        response.setAvailable(responseBackend.getEntityout().getIsavailable());
        response.setCreationDate(DateUtils.rebuildDateTime(responseBackend.getEntityout().getCreationdate()));
        response.setLastUpdateDate(DateUtils.rebuildDateTime(responseBackend.getEntityout().getLastupdatedate()));
        //alias
        response.setAlias(getAliasModel(responseBackend.getEntityout().getAlias()));
        //relatedContracts
        response.setRelatedContracts(getRelatedContractListModel(responseBackend.getEntityout().getRelatedcontracts()));
        return response;
    }

    private List<RelatedContract> getRelatedContractListModel(final List<Relatedcontracts> relatedcontracts) {
        if (CollectionUtils.isEmpty(relatedcontracts)) {
            return null;
        }
        return relatedcontracts.stream().filter(Objects::nonNull).map(this::getRelatedContractModel).collect(Collectors.toList());
    }

    private RelatedContract getRelatedContractModel(final Relatedcontracts relatedcontracts) {
        if (relatedcontracts.getRelatedcontract() == null) {
            return null;
        }
        RelatedContract response = new RelatedContract();
        response.setContractId(relatedcontracts.getRelatedcontract().getContractid());
        response.setNumber(relatedcontracts.getRelatedcontract().getNumber());
        response.setNumberType(getNumberTypeModel(relatedcontracts.getRelatedcontract().getNumbertype()));
        response.setProductType(getProductTypeModel(relatedcontracts.getRelatedcontract().getProducttype()));
        response.setRelationType(getRelationTypeModel(relatedcontracts.getRelatedcontract().getRelationtype()));
        return response;
    }

    private RelationType getRelationTypeModel(final Relationtype relationtype) {
        if (relationtype == null) {
            return null;
        }
        RelationType response = new RelationType();
        response.setId(relationtype.getId());
        response.setName(relationtype.getName());
        return response;
    }

    private NumberType getNumberTypeModel(final Numbertype numbertype) {
        if (numbertype == null) {
            return null;
        }
        NumberType response = new NumberType();
        response.setId(numbertype.getId());
        response.setDescription(numbertype.getDescription());
        return response;
    }

    private ProductType getProductTypeModel(final Producttype producttype) {
        if (producttype == null) {
            return null;
        }
        ProductType response = new ProductType();
        response.setId(producttype.getId());
        response.setDescription(producttype.getDescription());
        return response;
    }

    private com.bbva.pzic.peertopeer.facade.v0.dto.Alias getAliasModel(final Alias alias) {
        if (alias == null) {
            return null;
        }
        com.bbva.pzic.peertopeer.facade.v0.dto.Alias response = new com.bbva.pzic.peertopeer.facade.v0.dto.Alias();
        response.setId(alias.getId());
        response.setValue(alias.getValue());
        response.setAliasType(getAliasTypeModel(alias.getAliastype()));
        return response;
    }

    private AliasType getAliasTypeModel(final Aliastype aliastype) {
        if (aliastype == null) {
            return null;
        }
        AliasType response = new AliasType();
        response.setId(aliastype.getId());
        response.setDescription(aliastype.getDescription());
        return response;
    }

}
