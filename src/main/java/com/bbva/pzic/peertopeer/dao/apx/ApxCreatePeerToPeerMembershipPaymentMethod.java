package com.bbva.pzic.peertopeer.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxCreatePeerToPeerMembershipPaymentMethodMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.PeticionTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.RespuestaTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Component("apxCreatePeerToPeerMembershipPaymentMethod")
public class ApxCreatePeerToPeerMembershipPaymentMethod {
    private static final Log LOG = LogFactory.getLog(ApxCreatePeerToPeerMembershipPaymentMethod.class);

    @Resource(name = "apxCreatePeerToPeerMembershipPaymentMethodMapper")
    private IApxCreatePeerToPeerMembershipPaymentMethodMapper mapper;

    @Autowired
    private transient InvocadorTransaccion<PeticionTransaccionPmndt301_1, RespuestaTransaccionPmndt301_1> transaccion;

    public PaymentMethod invoke(final InputCreatePeerToPeerMembershipPaymentMethod input) {
        LOG.info("... Invoking method ApxGetPeerToPeerMembership.invoke...");
        PeticionTransaccionPmndt301_1 peticion = mapper.mapIn(input);
        RespuestaTransaccionPmndt301_1 respuesta = transaccion.invocar(peticion);
        return mapper.mapOut(respuesta);
    }
}
