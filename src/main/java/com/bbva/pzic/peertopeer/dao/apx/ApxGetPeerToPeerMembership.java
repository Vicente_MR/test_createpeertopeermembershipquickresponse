package com.bbva.pzic.peertopeer.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.peertopeer.business.dto.InputGetPeerToPeerMembership;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxGetPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.PeticionTransaccionPmndt101_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.RespuestaTransaccionPmndt101_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Component("apxGetPeerToPeerMembership")
public class ApxGetPeerToPeerMembership {
    private static final Log LOG = LogFactory.getLog(ApxGetPeerToPeerMembership.class);

    @Resource(name = "apxGetPeerToPeerMembershipMapper")
    private IApxGetPeerToPeerMembershipMapper mapper;

    @Autowired
    private transient InvocadorTransaccion<PeticionTransaccionPmndt101_1, RespuestaTransaccionPmndt101_1> transaccion;

    public Membership invoke(final InputGetPeerToPeerMembership input) {
        LOG.info("... Invoking method ApxGetPeerToPeerMembership.invoke...");
        PeticionTransaccionPmndt101_1 peticion = mapper.mapIn(input);
        RespuestaTransaccionPmndt101_1 respuesta = transaccion.invocar(peticion);
        return mapper.mapOut(respuesta);
    }

}
