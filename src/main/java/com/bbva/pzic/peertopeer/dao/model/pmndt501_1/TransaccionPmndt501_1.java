package com.bbva.pzic.peertopeer.dao.model.pmndt501_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PMNDT501</code>
 * 
 * @see PeticionTransaccionPmndt501_1
 * @see RespuestaTransaccionPmndt501_1
 */
@Component
public class TransaccionPmndt501_1 implements InvocadorTransaccion<PeticionTransaccionPmndt501_1,RespuestaTransaccionPmndt501_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPmndt501_1 invocar(PeticionTransaccionPmndt501_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt501_1.class, RespuestaTransaccionPmndt501_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPmndt501_1 invocarCache(PeticionTransaccionPmndt501_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt501_1.class, RespuestaTransaccionPmndt501_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}