package com.bbva.pzic.peertopeer.dao.model.pmndt301_1;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>relatedContract</code>, utilizado por la clase <code>Relatedcontracts</code></p>
 * 
 * @see Relatedcontracts
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Relatedcontract {
	
	/**
	 * <p>Campo <code>contractId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 1, nombre = "contractId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String contractid;
	
	/**
	 * <p>Campo <code>productType</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "productType", tipo = TipoCampo.DTO)
	private Producttype producttype;
	
	/**
	 * <p>Campo <code>relationType</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "relationType", tipo = TipoCampo.DTO)
	private Relationtype relationtype;
	
	/**
	 * <p>Campo <code>number</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 4, nombre = "number", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String number;
	
	/**
	 * <p>Campo <code>numberType</code>, &iacute;ndice: <code>5</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 5, nombre = "numberType", tipo = TipoCampo.DTO)
	private Numbertype numbertype;
	
}