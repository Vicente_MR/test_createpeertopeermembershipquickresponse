package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.business.dto.InputGetPeerToPeerMembership;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxGetPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.Alias;
import com.bbva.pzic.peertopeer.facade.v0.dto.Status;
import com.bbva.pzic.peertopeer.facade.v0.dto.*;
import com.bbva.pzic.peertopeer.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ApxGetPeerToPeerMembershipMapper implements IApxGetPeerToPeerMembershipMapper {
    @Override
    public PeticionTransaccionPmndt101_1 mapIn(InputGetPeerToPeerMembership input) {
        PeticionTransaccionPmndt101_1 peticion = new PeticionTransaccionPmndt101_1();
        peticion.setEntityin(new Entityin());
        peticion.getEntityin().setCustomerid(input.getCustomerId());
        peticion.getEntityin().setMembershipId(input.getMembershipId());
        peticion.getEntityin().setExpand(input.getExpand());
        return peticion;
    }

    @Override
    public Membership mapOut(RespuestaTransaccionPmndt101_1 responseBackend) {
        if (responseBackend == null || responseBackend.getEntityout() == null) {
            return null;
        }
        Membership membership = new Membership();
        membership.setId(responseBackend.getEntityout().getId());
        membership.setMembershipDate(DateUtils.rebuildDateTime(responseBackend.getEntityout().getMembershipdate()));
        membership.setLastUpdateDate(DateUtils.rebuildDateTime(responseBackend.getEntityout().getLastupdatedate()));
        membership.setIsPreferedReceiver(responseBackend.getEntityout().getIspreferedreceiver());
        //status
        membership.setStatus(getStatus(responseBackend.getEntityout().getStatus()));
        //paymentMethods
        membership.setPaymentMethods(getPaymentMethodList(responseBackend.getEntityout().getPaymentmethods()));
        return membership;
    }

    private List<PaymentMethod> getPaymentMethodList(final List<Paymentmethods> paymentmethods) {
        if (CollectionUtils.isEmpty(paymentmethods)) {
            return null;
        }
        return paymentmethods.stream().filter(Objects::nonNull).map(this::getPaymentMethod).collect(Collectors.toList());
    }

    private PaymentMethod getPaymentMethod(final Paymentmethods paymentmethods) {
        if (paymentmethods.getPaymentmethod() == null) {
            return null;
        }
        PaymentMethod response = new PaymentMethod();
        response.setId(paymentmethods.getPaymentmethod().getId());
        response.setAlias(getAlias(paymentmethods.getPaymentmethod().getAlias()));
        response.setCreationDate(DateUtils.rebuildDateTime(paymentmethods.getPaymentmethod().getCreationdate()));
        response.setLastUpdateDate(DateUtils.rebuildDateTime(paymentmethods.getPaymentmethod().getLastupdatedate()));
        response.setAvailable(paymentmethods.getPaymentmethod().getIsavailable());
        response.setRelatedContracts(getRelatedContracts(paymentmethods.getPaymentmethod().getRelatedcontracts()));
        return response;
    }

    private List<RelatedContract> getRelatedContracts(List<Relatedcontracts> relatedcontractsList) {
        if (CollectionUtils.isEmpty(relatedcontractsList)) {
            return null;
        }
        return relatedcontractsList.stream().filter(Objects::nonNull).map(this::getRelatedContract).collect(Collectors.toList());
    }

    private RelatedContract getRelatedContract(Relatedcontracts relatedcontracts) {
        if (relatedcontracts.getRelatedcontract() == null) {
            return null;
        }
        RelatedContract response = new RelatedContract();
        response.setContractId(relatedcontracts.getRelatedcontract().getContractid());
        response.setNumber(relatedcontracts.getRelatedcontract().getNumber());
        response.setNumberType(getNumberType(relatedcontracts.getRelatedcontract().getNumbertype()));
        response.setProductType(getProductType(relatedcontracts.getRelatedcontract().getProducttype()));
        response.setRelationType(getRelationType(relatedcontracts.getRelatedcontract().getRelationtype()));
        return response;
    }

    private RelationType getRelationType(Relationtype relationtype) {
        if (relationtype == null) {
            return null;
        }
        RelationType response = new RelationType();
        response.setId(relationtype.getId());
        response.setName(relationtype.getName());
        return response;
    }

    private NumberType getNumberType(Numbertype numbertype) {
        if (numbertype == null) {
            return null;
        }
        NumberType response = new NumberType();
        response.setId(numbertype.getId());
        response.setDescription(numbertype.getDescription());
        return response;
    }

    private ProductType getProductType(Producttype producttype) {
        if (producttype == null) {
            return null;
        }
        ProductType response = new ProductType();
        response.setId(producttype.getId());
        response.setDescription(producttype.getDescription());
        return response;
    }

    private Alias getAlias(com.bbva.pzic.peertopeer.dao.model.pmndt101_1.Alias alias) {
        if (alias == null) {
            return null;
        }
        Alias response = new Alias();
        response.setId(alias.getId());
        response.setValue(alias.getValue());
        response.setAliasType(getAliasType(alias.getAliastype()));
        return response;
    }

    private AliasType getAliasType(Aliastype aliastype) {
        if (aliastype == null) {
            return null;
        }
        AliasType response = new AliasType();
        response.setId(aliastype.getId());
        response.setDescription(aliastype.getDescription());
        return response;
    }

    private Status getStatus(final com.bbva.pzic.peertopeer.dao.model.pmndt101_1.Status status) {
        if (status == null) {
            return null;
        }
        Status statusResponse = new Status();
        statusResponse.setId(status.getId());
        statusResponse.setDescription(status.getDescription());
        return statusResponse;
    }
}
