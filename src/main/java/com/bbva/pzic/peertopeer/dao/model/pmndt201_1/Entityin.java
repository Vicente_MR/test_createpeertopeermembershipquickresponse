package com.bbva.pzic.peertopeer.dao.model.pmndt201_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>EntityIn</code>, utilizado por la clase <code>PeticionTransaccionPmndt201_1</code></p>
 * 
 * @see PeticionTransaccionPmndt201_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityin {
	
	/**
	 * <p>Campo <code>customerId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "customerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
	private String customerid;
	
	/**
	 * <p>Campo <code>membership-id</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "membership-id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
	private String membershipId;
	
	/**
	 * <p>Campo <code>status</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "status", tipo = TipoCampo.DTO)
	private Status status;
	
	/**
	 * <p>Campo <code>isPreferedReceiver</code>, &iacute;ndice: <code>4</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 4, nombre = "isPreferedReceiver", tipo = TipoCampo.BOOLEAN, signo = true)
	private Boolean ispreferedreceiver;
	
}