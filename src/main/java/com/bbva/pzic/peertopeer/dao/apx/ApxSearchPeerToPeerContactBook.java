package com.bbva.pzic.peertopeer.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.peertopeer.business.dto.InputSearchPeerToPeerContactBook;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxSearchPeerToPeerContactBookMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.PeticionTransaccionPmndt401_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.RespuestaTransaccionPmndt401_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 2/26/2020.
 *
 * @author Cesardl
 */
@Component("apxSearchPeerToPeerContactBook")
public class ApxSearchPeerToPeerContactBook {

    private static final Log LOG = LogFactory.getLog(ApxModifyPeerToPeerMembershipPaymentMethod.class);

    @Resource(name = "apxSearchPeerToPeerContactBookMapper")
    private IApxSearchPeerToPeerContactBookMapper mapper;

    @Autowired
    private transient InvocadorTransaccion<PeticionTransaccionPmndt401_1, RespuestaTransaccionPmndt401_1> transaccion;

    public ContactsBook invoke(final InputSearchPeerToPeerContactBook input) {
        LOG.info("... Invoking method ApxGetPeerToPeerMembership.invoke...");
        PeticionTransaccionPmndt401_1 peticion = mapper.mapIn(input);
        RespuestaTransaccionPmndt401_1 respuesta = transaccion.invocar(peticion);
        return mapper.mapOut(respuesta);
    }
}
