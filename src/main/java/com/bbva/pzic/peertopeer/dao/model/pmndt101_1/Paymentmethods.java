package com.bbva.pzic.peertopeer.dao.model.pmndt101_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>paymentMethods</code>, utilizado por la clase <code>Entityout</code></p>
 *
 * @see Entityout
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Paymentmethods {

	/**
	 * <p>Campo <code>paymentMethod</code>, &iacute;ndice: <code>0</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "paymentMethod", tipo = TipoCampo.DTO)
	private Paymentmethod paymentmethod;

}
