// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.peertopeer.dao.model.pmndt301_1;

import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.Producttype;

privileged aspect Producttype_Roo_JavaBean {
    
    /**
     * Gets id value
     * 
     * @return String
     */
    public String Producttype.getId() {
        return this.id;
    }
    
    /**
     * Sets id value
     * 
     * @param id
     * @return Producttype
     */
    public Producttype Producttype.setId(String id) {
        this.id = id;
        return this;
    }
    
    /**
     * Gets description value
     * 
     * @return String
     */
    public String Producttype.getDescription() {
        return this.description;
    }
    
    /**
     * Sets description value
     * 
     * @param description
     * @return Producttype
     */
    public Producttype Producttype.setDescription(String description) {
        this.description = description;
        return this;
    }
    
}
