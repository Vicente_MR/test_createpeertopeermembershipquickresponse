package com.bbva.pzic.peertopeer.dao.model.pmndt101_1.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.PeticionTransaccionPmndt101_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.RespuestaTransaccionPmndt101_1;
import com.bbva.pzic.peertopeer.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TransaccionPmndt101_1Mock implements InvocadorTransaccion<PeticionTransaccionPmndt101_1, RespuestaTransaccionPmndt101_1> {
    public static final String TEST_EMPTY = "999";
    public static final String TEST_WITHOUT_EXPAND = "888";
    public static final String TEST_RESPONSE_NULL = "777";
    @Override
    public RespuestaTransaccionPmndt101_1 invocar(PeticionTransaccionPmndt101_1 request) throws ExcepcionTransaccion {
        try {
            RespuestaTransaccionPmndt101_1 respuesta = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt101_1();
            if (request.getEntityin().getMembershipId().equalsIgnoreCase(TEST_EMPTY)) {
                return new RespuestaTransaccionPmndt101_1();
            }else if (request.getEntityin().getMembershipId().equalsIgnoreCase(TEST_WITHOUT_EXPAND)){
                respuesta.getEntityout().setPaymentmethods(null);
                return respuesta;
            }else if (request.getEntityin().getMembershipId().equalsIgnoreCase(TEST_RESPONSE_NULL)){
                return null;
            }
            return respuesta;

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPmndt101_1 invocarCache(PeticionTransaccionPmndt101_1 peticionTransaccionPmndt101_1) {
        return null;
    }

    @Override
    public void vaciarCache() {

    }
}
