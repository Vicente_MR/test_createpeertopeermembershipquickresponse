package com.bbva.pzic.peertopeer.dao.model.pmndt401_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PMNDT401</code>
 * 
 * @see PeticionTransaccionPmndt401_1
 * @see RespuestaTransaccionPmndt401_1
 */
@Component
public class TransaccionPmndt401_1 implements InvocadorTransaccion<PeticionTransaccionPmndt401_1,RespuestaTransaccionPmndt401_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPmndt401_1 invocar(PeticionTransaccionPmndt401_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt401_1.class, RespuestaTransaccionPmndt401_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPmndt401_1 invocarCache(PeticionTransaccionPmndt401_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt401_1.class, RespuestaTransaccionPmndt401_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}