package com.bbva.pzic.peertopeer.dao.impl;

import com.bbva.pzic.peertopeer.business.dto.*;
import com.bbva.pzic.peertopeer.dao.IPeerToPeerDAO;
import com.bbva.pzic.peertopeer.dao.apx.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Repository
public class PeerToPeerDAO implements IPeerToPeerDAO {

    private static final Log LOG = LogFactory.getLog(PeerToPeerDAO.class);

    @Autowired
    private ApxGetPeerToPeerMembership apxGetPeerToPeerMembership;
    @Autowired
    private ApxModifyPeerToPeerMembership apxModifyPeerToPeerMembership;
    @Autowired
    private ApxCreatePeerToPeerMembershipPaymentMethod apxCreatePeerToPeerMembershipPaymentMethod;
    @Autowired
    private ApxModifyPeerToPeerMembershipPaymentMethod apxModifyPeerToPeerMembershipPaymentMethod;
    @Autowired
    private ApxSearchPeerToPeerContactBook apxSearchPeerToPeerContactBook;
    @Autowired
    private ApxCreatePeerToPeerMembershipQuickResponse apxCreatePeerToPeerMembershipQuickResponse;

    /**
     * {@inheritDoc}
     */
    @Override
    public Membership getPeerToPeerMembership(
            final InputGetPeerToPeerMembership input) {
        LOG.info("... Invoking method PeerToPeerDAO.getPeerToPeerMembership ...");
        return apxGetPeerToPeerMembership.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Membership modifyPeerToPeerMembership(
            final InputModifyPeerToPeerMembership input) {
        LOG.info("... Invoking method PeerToPeerDAO.modifyPeerToPeerMembership ...");
        return apxModifyPeerToPeerMembership.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentMethod createPeerToPeerMembershipPaymentMethod(
            final InputCreatePeerToPeerMembershipPaymentMethod input) {
        LOG.info("... Invoking method PeerToPeerDAO.createPeerToPeerMembershipPaymentMethod ...");
        return apxCreatePeerToPeerMembershipPaymentMethod.invoke(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentMethod modifyPeerToPeerMembershipPaymentMethod(
            final InputModifyPeerToPeerMembershipPaymentMethod input) {
        LOG.info("... Invoking method PeerToPeerDAO.modifyPeerToPeerMembershipPaymentMethod ...");
        return apxModifyPeerToPeerMembershipPaymentMethod.invoke(input);
    }

    @Override
    public ContactsBook searchPeerToPeerContactBook(final InputSearchPeerToPeerContactBook input) {
        LOG.info("... Invoking method PeerToPeerDAO.searchPeerToPeerContactBook ...");
        return apxSearchPeerToPeerContactBook.invoke(input);
    }

    @Override
    public QuickResponse createPeerToPeerMembershipQuickResponse(final InputCreatePeerToPeerMembershipQuickResponse input) {
        LOG.info("... Invoking method PeerToPeerDAO.createPeerToPeerMembershipQuickREsponse ...");
        return apxCreatePeerToPeerMembershipQuickResponse.invoke(input);
    }
}
