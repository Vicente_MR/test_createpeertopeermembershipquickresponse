package com.bbva.pzic.peertopeer.dao.model.pmndt101_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>relatedContracts</code>, utilizado por la clase <code>Paymentmethod</code></p>
 * 
 * @see Paymentmethod
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Relatedcontracts {
	
	/**
	 * <p>Campo <code>relatedContract</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "relatedContract", tipo = TipoCampo.DTO)
	private Relatedcontract relatedcontract;
	
}