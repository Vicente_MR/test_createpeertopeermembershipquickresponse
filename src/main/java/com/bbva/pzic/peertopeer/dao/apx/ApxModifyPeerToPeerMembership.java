package com.bbva.pzic.peertopeer.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembership;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxModifyPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.PeticionTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.RespuestaTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Component("apxModifyPeerToPeerMembership")
public class ApxModifyPeerToPeerMembership {
    private static final Log LOG = LogFactory.getLog(ApxModifyPeerToPeerMembership.class);

    @Resource(name = "apxModifyPeerToPeerMembershipMapper")
    private IApxModifyPeerToPeerMembershipMapper mapper;

    @Autowired
    private transient InvocadorTransaccion<PeticionTransaccionPmndt201_1, RespuestaTransaccionPmndt201_1> transaccion;

    public Membership invoke(final InputModifyPeerToPeerMembership input) {
        LOG.info("... Invoking method ApxGetPeerToPeerMembership.invoke...");
        PeticionTransaccionPmndt201_1 peticion = mapper.mapIn(input);
        RespuestaTransaccionPmndt201_1 respuesta = transaccion.invocar(peticion);
        return mapper.mapOut(respuesta);
    }
}
