package com.bbva.pzic.peertopeer.dao.model.pmndt301_1.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.PeticionTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.RespuestaTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TransaccionPmndt301_1Mock implements InvocadorTransaccion<PeticionTransaccionPmndt301_1, RespuestaTransaccionPmndt301_1> {
    public static final String TEST_EMPTY = "999";
    public static final String TEST_RESPONSE_NULL = "777";

    @Override
    public RespuestaTransaccionPmndt301_1 invocar(PeticionTransaccionPmndt301_1 request) throws ExcepcionTransaccion {
        try {
            RespuestaTransaccionPmndt301_1 respuesta = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt301_1();
            if (request.getEntityinput().getMembershipId().equalsIgnoreCase(TEST_EMPTY)) {
                return new RespuestaTransaccionPmndt301_1();
            }else if (request.getEntityinput().getMembershipId().equalsIgnoreCase(TEST_RESPONSE_NULL)){
                return null;
            }
            return respuesta;

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPmndt301_1 invocarCache(PeticionTransaccionPmndt301_1 peticionTransaccionPmndt301_1) {
        return null;
    }

    @Override
    public void vaciarCache() {

    }
}
