package com.bbva.pzic.peertopeer.dao.model.pmndt201_1;

import java.util.Calendar;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>EntityOut</code>, utilizado por la clase <code>RespuestaTransaccionPmndt201_1</code></p>
 * 
 * @see RespuestaTransaccionPmndt201_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityout {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>isPreferedReceiver</code>, &iacute;ndice: <code>2</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 2, nombre = "isPreferedReceiver", tipo = TipoCampo.BOOLEAN, signo = true, obligatorio = true)
	private Boolean ispreferedreceiver;
	
	/**
	 * <p>Campo <code>status</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "status", tipo = TipoCampo.DTO)
	private Status status;
	
	/**
	 * <p>Campo <code>membershipDate</code>, &iacute;ndice: <code>4</code>, tipo: <code>TIMESTAMP</code>
	 */
	@Campo(indice = 4, nombre = "membershipDate", tipo = TipoCampo.TIMESTAMP, signo = true, formato = "yyyy-MM-dd'T'HH:mm:ss.SSSX", obligatorio = true)
	private Calendar membershipdate;
	
	/**
	 * <p>Campo <code>lastUpdateDate</code>, &iacute;ndice: <code>5</code>, tipo: <code>TIMESTAMP</code>
	 */
	@Campo(indice = 5, nombre = "lastUpdateDate", tipo = TipoCampo.TIMESTAMP, signo = true, formato = "yyyy-MM-dd'T'HH:mm:ss.SSSX", obligatorio = true)
	private Calendar lastupdatedate;
	
}