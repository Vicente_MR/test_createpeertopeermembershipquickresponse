package com.bbva.pzic.peertopeer.dao.model.pmndt101_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PMNDT101</code>
 * 
 * @see PeticionTransaccionPmndt101_1
 * @see RespuestaTransaccionPmndt101_1
 */
@Component
public class TransaccionPmndt101_1 implements InvocadorTransaccion<PeticionTransaccionPmndt101_1,RespuestaTransaccionPmndt101_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPmndt101_1 invocar(PeticionTransaccionPmndt101_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt101_1.class, RespuestaTransaccionPmndt101_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPmndt101_1 invocarCache(PeticionTransaccionPmndt101_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt101_1.class, RespuestaTransaccionPmndt101_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}