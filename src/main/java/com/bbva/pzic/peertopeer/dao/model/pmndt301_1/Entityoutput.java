package com.bbva.pzic.peertopeer.dao.model.pmndt301_1;

import java.util.Calendar;
import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>entityOutput</code>, utilizado por la clase <code>RespuestaTransaccionPmndt301_1</code></p>
 * 
 * @see RespuestaTransaccionPmndt301_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityoutput {
	
	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>alias</code>, &iacute;ndice: <code>2</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 2, nombre = "alias", tipo = TipoCampo.DTO)
	private Alias alias;
	
	/**
	 * <p>Campo <code>relatedContracts</code>, &iacute;ndice: <code>3</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 3, nombre = "relatedContracts", tipo = TipoCampo.LIST)
	private List<Relatedcontracts> relatedcontracts;
	
	/**
	 * <p>Campo <code>isAvailable</code>, &iacute;ndice: <code>4</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 4, nombre = "isAvailable", tipo = TipoCampo.BOOLEAN, signo = true, obligatorio = true)
	private Boolean isavailable;
	
	/**
	 * <p>Campo <code>lastUpdateDate</code>, &iacute;ndice: <code>5</code>, tipo: <code>TIMESTAMP</code>
	 */
	@Campo(indice = 5, nombre = "lastUpdateDate", tipo = TipoCampo.TIMESTAMP, signo = true, formato = "yyyy-MM-dd'T'HH:mm:ss.SSSX", obligatorio = true)
	private Calendar lastupdatedate;
	
	/**
	 * <p>Campo <code>creationDate</code>, &iacute;ndice: <code>6</code>, tipo: <code>TIMESTAMP</code>
	 */
	@Campo(indice = 6, nombre = "creationDate", tipo = TipoCampo.TIMESTAMP, signo = true, formato = "yyyy-MM-dd'T'HH:mm:ss.SSSX", obligatorio = true)
	private Calendar creationdate;
	
}