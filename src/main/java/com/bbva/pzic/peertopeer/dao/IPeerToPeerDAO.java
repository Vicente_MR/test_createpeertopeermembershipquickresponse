package com.bbva.pzic.peertopeer.dao;

import com.bbva.pzic.peertopeer.business.dto.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface IPeerToPeerDAO {

    Membership getPeerToPeerMembership(InputGetPeerToPeerMembership input);

    Membership modifyPeerToPeerMembership(InputModifyPeerToPeerMembership input);

    PaymentMethod createPeerToPeerMembershipPaymentMethod(InputCreatePeerToPeerMembershipPaymentMethod input);

    PaymentMethod modifyPeerToPeerMembershipPaymentMethod(InputModifyPeerToPeerMembershipPaymentMethod input);

    ContactsBook searchPeerToPeerContactBook(InputSearchPeerToPeerContactBook input);

    QuickResponse createPeerToPeerMembershipQuickResponse(InputCreatePeerToPeerMembershipQuickResponse input);
}
