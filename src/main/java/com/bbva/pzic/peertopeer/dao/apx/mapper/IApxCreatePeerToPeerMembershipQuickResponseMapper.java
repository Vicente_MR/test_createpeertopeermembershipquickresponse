package com.bbva.pzic.peertopeer.dao.apx.mapper;

import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipQuickResponse;
import com.bbva.pzic.peertopeer.dao.model.pmndt501_1.PeticionTransaccionPmndt501_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt501_1.RespuestaTransaccionPmndt501_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;

/**
 * Created on 07/01/2020.
 *
 * @author Entelgy
 */
public interface IApxCreatePeerToPeerMembershipQuickResponseMapper {

    PeticionTransaccionPmndt501_1 mapIn(InputCreatePeerToPeerMembershipQuickResponse input);

    QuickResponse mapOut(RespuestaTransaccionPmndt501_1 responseBackend);
}
