package com.bbva.pzic.peertopeer.dao.apx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipQuickResponse;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxCreatePeerToPeerMembershipQuickResponseMapper;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxModifyPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt501_1.PeticionTransaccionPmndt501_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt501_1.RespuestaTransaccionPmndt501_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 01/07/2020.
 *
 * @author Entelgy
 */
@Component
public class ApxCreatePeerToPeerMembershipQuickResponse {

    private static final Log LOG = LogFactory.getLog(ApxCreatePeerToPeerMembershipQuickResponse.class);

    @Autowired
    private IApxCreatePeerToPeerMembershipQuickResponseMapper mapper;

    @Autowired
    private InvocadorTransaccion<PeticionTransaccionPmndt501_1, RespuestaTransaccionPmndt501_1> transaccion;

    public QuickResponse invoke(final InputCreatePeerToPeerMembershipQuickResponse input) {
        LOG.info("... Invoking method ApxCreatePeerToPeerMembershipQuickResponse.invoke...");

        PeticionTransaccionPmndt501_1 peticion = mapper.mapIn(input);
        RespuestaTransaccionPmndt501_1 respuesta = transaccion.invocar(peticion);
        return mapper.mapOut(respuesta);
    }

}
