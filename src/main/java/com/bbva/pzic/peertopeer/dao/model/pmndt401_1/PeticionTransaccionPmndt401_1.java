package com.bbva.pzic.peertopeer.dao.model.pmndt401_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PMNDT401</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPmndt401_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPmndt401_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PMNDT401-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PMNDT401&quot; application=&quot;PMND&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;dto name=&quot;entityIn&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.PeerToPeerContactBookDTO&quot; artifactId=&quot;PMNDC400&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;customerId&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;list name=&quot;contacts&quot; order=&quot;2&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;contact&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.ContactDTO&quot; artifactId=&quot;PMNDC400&quot;
 * mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;contactType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.ContactTypeDTO&quot;
 * artifactId=&quot;PMNDC400&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;value&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;firstName&quot; type=&quot;String&quot; size=&quot;35&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;lastName&quot; type=&quot;String&quot; size=&quot;35&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;isActive&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto name=&quot;entityOut&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.PeerToPeerContactBookDTO&quot; artifactId=&quot;PMNDC400&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;data&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.DataDTO&quot; artifactId=&quot;PMNDC400&quot; mandatory=&quot;1&quot;
 * order=&quot;1&quot;&gt;
 * &lt;list name=&quot;contacts&quot; order=&quot;1&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;contact&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.ContactDTO&quot; artifactId=&quot;PMNDC400&quot;
 * mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;dto name=&quot;contactType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.ContactTypeDTO&quot;
 * artifactId=&quot;PMNDC400&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;value&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;firstName&quot; type=&quot;String&quot; size=&quot;35&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;lastName&quot; type=&quot;String&quot; size=&quot;35&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;isActive&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;-&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPmndt401_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PMNDT401",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPmndt401_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPmndt401_1 {
		
		/**
	 * <p>Campo <code>entityIn</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "entityIn", tipo = TipoCampo.DTO)
	private Entityin entityin;
	
}