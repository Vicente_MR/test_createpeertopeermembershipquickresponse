package com.bbva.pzic.peertopeer.dao.apx.mapper;

import com.bbva.pzic.peertopeer.business.dto.InputGetPeerToPeerMembership;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.PeticionTransaccionPmndt101_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.RespuestaTransaccionPmndt101_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface IApxGetPeerToPeerMembershipMapper {
    PeticionTransaccionPmndt101_1 mapIn(InputGetPeerToPeerMembership input);

    Membership mapOut(RespuestaTransaccionPmndt101_1 responseBackend);
}
