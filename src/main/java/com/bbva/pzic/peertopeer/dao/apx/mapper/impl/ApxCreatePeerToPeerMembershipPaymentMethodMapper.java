package com.bbva.pzic.peertopeer.dao.apx.mapper.impl;

import com.bbva.pzic.peertopeer.business.dto.*;
import com.bbva.pzic.peertopeer.dao.apx.mapper.IApxCreatePeerToPeerMembershipPaymentMethodMapper;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.Alias;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.*;
import com.bbva.pzic.peertopeer.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ApxCreatePeerToPeerMembershipPaymentMethodMapper implements IApxCreatePeerToPeerMembershipPaymentMethodMapper {
    @Override
    public PeticionTransaccionPmndt301_1 mapIn(InputCreatePeerToPeerMembershipPaymentMethod input) {
        PeticionTransaccionPmndt301_1 request = new PeticionTransaccionPmndt301_1();
        request.setEntityinput(new Entityinput());
        request.getEntityinput().setCustomerid(input.getCustomerId());
        request.getEntityinput().setMembershipId(input.getMembershipId());
        //alias
        request.getEntityinput().setAlias(getAlias(input.getPaymentMethod().getAlias()));
        //relatedContracts
        request.getEntityinput().setRelatedcontracts(getRelatedcontractsList(input.getPaymentMethod().getRelatedContracts()));
        return request;
    }

    @Override
    public PaymentMethod mapOut(RespuestaTransaccionPmndt301_1 responseBackend) {
        if (responseBackend == null || responseBackend.getEntityoutput() == null) {
            return null;
        }

        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(responseBackend.getEntityoutput().getId());
        paymentMethod.setAvailable(responseBackend.getEntityoutput().getIsavailable());
        paymentMethod.setLastUpdateDate(DateUtils.rebuildDateTime(responseBackend.getEntityoutput().getLastupdatedate()));
        paymentMethod.setCreationDate(DateUtils.rebuildDateTime(responseBackend.getEntityoutput().getCreationdate()));
        //alias
        paymentMethod.setAlias(getAliasResponse(responseBackend.getEntityoutput().getAlias()));
        //relatedContracts
        paymentMethod.setRelatedContracts(getRelatedContractList(responseBackend.getEntityoutput().getRelatedcontracts()));
        return paymentMethod;
    }

    private List<RelatedContract> getRelatedContractList(List<Relatedcontracts> relatedcontracts) {
        if (CollectionUtils.isEmpty(relatedcontracts)) {
            return null;
        }
        return relatedcontracts.stream().filter(Objects::nonNull).map(this::getRelatedContract).collect(Collectors.toList());
    }

    private RelatedContract getRelatedContract(Relatedcontracts relatedcontracts) {
        if (relatedcontracts.getRelatedcontract() == null) {
            return null;
        }
        RelatedContract response = new RelatedContract();
        response.setContractId(relatedcontracts.getRelatedcontract().getContractid());
        response.setNumber(relatedcontracts.getRelatedcontract().getNumber());
        response.setNumberType(getNumberType(relatedcontracts.getRelatedcontract().getNumbertype()));
        response.setProductType(getProductType(relatedcontracts.getRelatedcontract().getProducttype()));
        response.setRelationType(getRelationType(relatedcontracts.getRelatedcontract().getRelationtype()));
        return response;
    }

    private RelationType getRelationType(Relationtype relationtype) {
        if (relationtype == null) {
            return null;
        }
        RelationType response = new RelationType();
        response.setId(relationtype.getId());
        response.setName(relationtype.getName());
        return response;
    }

    private NumberType getNumberType(Numbertype numbertype) {
        if (numbertype == null) {
            return null;
        }
        NumberType response = new NumberType();
        response.setId(numbertype.getId());
        response.setDescription(numbertype.getDescription());
        return response;
    }

    private ProductType getProductType(Producttype producttype) {
        if (producttype == null) {
            return null;
        }
        ProductType response = new ProductType();
        response.setId(producttype.getId());
        response.setDescription(producttype.getDescription());
        return response;
    }

    private com.bbva.pzic.peertopeer.facade.v0.dto.Alias getAliasResponse(Alias alias) {
        if (alias == null) {
            return null;
        }
        com.bbva.pzic.peertopeer.facade.v0.dto.Alias response = new com.bbva.pzic.peertopeer.facade.v0.dto.Alias();
        response.setId(alias.getId());
        response.setValue(alias.getValue());
        response.setAliasType(getAliasType(alias.getAliastype()));
        return response;
    }

    private AliasType getAliasType(Aliastype aliastype) {
        if (aliastype == null) {
            return null;
        }
        AliasType response = new AliasType();
        response.setId(aliastype.getId());
        response.setDescription(aliastype.getDescription());
        return response;
    }

    private List<Relatedcontracts> getRelatedcontractsList(List<DTOIntRelatedContract> relatedContractList) {
        return relatedContractList.stream().filter(Objects::nonNull).map(this::getRelatedcontracts).collect(Collectors.toList());
    }

    private Relatedcontracts getRelatedcontracts(DTOIntRelatedContract relatedContract) {
        Relatedcontracts request = new Relatedcontracts();
        request.setRelatedcontract(new Relatedcontract());
        request.getRelatedcontract().setContractid(relatedContract.getContractId());
        request.getRelatedcontract().setProducttype(getProducttype(relatedContract.getProductType()));
        request.getRelatedcontract().setRelationtype(getRelationtype(relatedContract.getRelationType()));
        return request;
    }

    private Relationtype getRelationtype(DTOIntRelationType relationType) {
        if (relationType == null) {
            return null;
        }
        Relationtype request = new Relationtype();
        request.setId(relationType.getId());
        return request;
    }

    private Producttype getProducttype(DTOIntProductType productType) {
        if (productType == null) {
            return null;
        }
        Producttype request = new Producttype();
        request.setId(productType.getId());
        return request;
    }

    private Alias getAlias(DTOIntAlias alias) {
        Alias request = new Alias();
        request.setId(alias.getId());
        request.setValue(alias.getValue());
        request.setAliastype(getAliastype(alias.getAliasType()));

        return request;
    }

    private Aliastype getAliastype(DTOIntAliasType aliasType) {
        Aliastype request = new Aliastype();
        request.setId(aliasType.getId());
        return request;
    }
}
