package com.bbva.pzic.peertopeer.dao.model.pmndt401_1;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>contact</code>, utilizado por la clase <code>Contacts</code></p>
 *
 * @see Contacts
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Contact {

	/**
	 * <p>Campo <code>contactType</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "contactType", tipo = TipoCampo.DTO)
	private Contacttype contacttype;

	/**
	 * <p>Campo <code>value</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 2, nombre = "value", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
	private String value;

	/**
	 * <p>Campo <code>firstName</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 3, nombre = "firstName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 35, signo = true)
	private String firstname;

	/**
	 * <p>Campo <code>lastName</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 4, nombre = "lastName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 35, signo = true)
	private String lastname;

	/**
	 * <p>Campo <code>isActive</code>, &iacute;ndice: <code>5</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 5, nombre = "isActive", tipo = TipoCampo.BOOLEAN, signo = true, obligatorio = true)
	private Boolean isactive;

}
