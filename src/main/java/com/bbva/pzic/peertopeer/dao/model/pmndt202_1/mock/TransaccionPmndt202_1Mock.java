package com.bbva.pzic.peertopeer.dao.model.pmndt202_1.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.peertopeer.dao.model.mock.stubs.EntityResponseBackendApxStubs;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.PeticionTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.RespuestaTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.util.Errors;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TransaccionPmndt202_1Mock implements InvocadorTransaccion<PeticionTransaccionPmndt202_1, RespuestaTransaccionPmndt202_1> {
    public static final String TEST_EMPTY = "999";
    public static final String TEST_RESPONSE_NULL = "777";

    @Override
    public RespuestaTransaccionPmndt202_1 invocar(PeticionTransaccionPmndt202_1 request) throws ExcepcionTransaccion {
        try {
            RespuestaTransaccionPmndt202_1 respuesta = EntityResponseBackendApxStubs.getInstance().buildRespuestaTransaccionPmndt202_1();
            if (request.getEntityin().getMembershipId().equalsIgnoreCase(TEST_EMPTY)) {
                return new RespuestaTransaccionPmndt202_1();
            }else if (request.getEntityin().getMembershipId().equalsIgnoreCase(TEST_RESPONSE_NULL)){
                return null;
            }
            return respuesta;

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }

    @Override
    public RespuestaTransaccionPmndt202_1 invocarCache(PeticionTransaccionPmndt202_1 peticionTransaccionPmndt202_1) {
        return null;
    }

    @Override
    public void vaciarCache() {

    }
}
