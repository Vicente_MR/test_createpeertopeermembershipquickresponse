package com.bbva.pzic.peertopeer.dao.apx.mapper;

import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembership;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.PeticionTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.RespuestaTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface IApxModifyPeerToPeerMembershipMapper {
    PeticionTransaccionPmndt201_1 mapIn(InputModifyPeerToPeerMembership input);

    Membership mapOut(RespuestaTransaccionPmndt201_1 responseBackend);
}
