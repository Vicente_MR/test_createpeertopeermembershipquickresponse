// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.peertopeer.dao.model.pmndt301_1;

import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.Entityoutput;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.RespuestaTransaccionPmndt301_1;

privileged aspect RespuestaTransaccionPmndt301_1_Roo_JavaBean {
    
    /**
     * Gets codigoAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPmndt301_1.getCodigoAviso() {
        return this.codigoAviso;
    }
    
    /**
     * Sets codigoAviso value
     * 
     * @param codigoAviso
     * @return RespuestaTransaccionPmndt301_1
     */
    public RespuestaTransaccionPmndt301_1 RespuestaTransaccionPmndt301_1.setCodigoAviso(String codigoAviso) {
        this.codigoAviso = codigoAviso;
        return this;
    }
    
    /**
     * Gets descripcionAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPmndt301_1.getDescripcionAviso() {
        return this.descripcionAviso;
    }
    
    /**
     * Sets descripcionAviso value
     * 
     * @param descripcionAviso
     * @return RespuestaTransaccionPmndt301_1
     */
    public RespuestaTransaccionPmndt301_1 RespuestaTransaccionPmndt301_1.setDescripcionAviso(String descripcionAviso) {
        this.descripcionAviso = descripcionAviso;
        return this;
    }
    
    /**
     * Gets aplicacionAviso value
     * 
     * @return String
     */
    public String RespuestaTransaccionPmndt301_1.getAplicacionAviso() {
        return this.aplicacionAviso;
    }
    
    /**
     * Sets aplicacionAviso value
     * 
     * @param aplicacionAviso
     * @return RespuestaTransaccionPmndt301_1
     */
    public RespuestaTransaccionPmndt301_1 RespuestaTransaccionPmndt301_1.setAplicacionAviso(String aplicacionAviso) {
        this.aplicacionAviso = aplicacionAviso;
        return this;
    }
    
    /**
     * Gets codigoRetorno value
     * 
     * @return String
     */
    public String RespuestaTransaccionPmndt301_1.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    /**
     * Sets codigoRetorno value
     * 
     * @param codigoRetorno
     * @return RespuestaTransaccionPmndt301_1
     */
    public RespuestaTransaccionPmndt301_1 RespuestaTransaccionPmndt301_1.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
        return this;
    }
    
    /**
     * Gets entityoutput value
     * 
     * @return Entityoutput
     */
    public Entityoutput RespuestaTransaccionPmndt301_1.getEntityoutput() {
        return this.entityoutput;
    }
    
    /**
     * Sets entityoutput value
     * 
     * @param entityoutput
     * @return RespuestaTransaccionPmndt301_1
     */
    public RespuestaTransaccionPmndt301_1 RespuestaTransaccionPmndt301_1.setEntityoutput(Entityoutput entityoutput) {
        this.entityoutput = entityoutput;
        return this;
    }
    
}
