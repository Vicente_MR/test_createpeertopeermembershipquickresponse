package com.bbva.pzic.peertopeer.dao.model.mock.stubs;

import com.bbva.pzic.peertopeer.dao.model.pmndt101_1.RespuestaTransaccionPmndt101_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt201_1.RespuestaTransaccionPmndt201_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.RespuestaTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt301_1.RespuestaTransaccionPmndt301_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt401_1.RespuestaTransaccionPmndt401_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt501_1.RespuestaTransaccionPmndt501_1;
import com.bbva.pzic.peertopeer.util.mappers.ObjectMapperHelper;

import java.io.IOException;

public final class EntityResponseBackendApxStubs {
    private static final EntityResponseBackendApxStubs INSTANCE = new EntityResponseBackendApxStubs();
    private ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private EntityResponseBackendApxStubs() {
    }

    public static EntityResponseBackendApxStubs getInstance() {
        return INSTANCE;
    }

    public RespuestaTransaccionPmndt101_1 buildRespuestaTransaccionPmndt101_1()throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/peertopeer/dao/model/mock/stubs/respuestaTransaccionPmndt101_1.json"),
                RespuestaTransaccionPmndt101_1.class);
    }

    public RespuestaTransaccionPmndt201_1 buildRespuestaTransaccionPmndt201_1()throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/peertopeer/dao/model/mock/stubs/respuestaTransaccionPmndt201_1.json"),
                RespuestaTransaccionPmndt201_1.class);
    }

    public RespuestaTransaccionPmndt202_1 buildRespuestaTransaccionPmndt202_1()throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/peertopeer/dao/model/mock/stubs/respuestaTransaccionPmndt202_1.json"),
                RespuestaTransaccionPmndt202_1.class);
    }

    public RespuestaTransaccionPmndt301_1 buildRespuestaTransaccionPmndt301_1()throws IOException{
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/peertopeer/dao/model/mock/stubs/respuestaTransaccionPmndt301_1.json"),
                RespuestaTransaccionPmndt301_1.class);
    }

    public RespuestaTransaccionPmndt401_1 buildRespuestaTransaccionPmndt401_1()throws IOException{
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/peertopeer/dao/model/mock/stubs/respuestaTransaccionPmndt401_1.json"),
                RespuestaTransaccionPmndt401_1.class);
    }

    public RespuestaTransaccionPmndt501_1 buildRespuestaTransaccionPmndt502_1() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/peertopeer/dao/model/mock/stubs/respuestaTransaccionPmndt501_1.json"), RespuestaTransaccionPmndt501_1.class);
    }
}
