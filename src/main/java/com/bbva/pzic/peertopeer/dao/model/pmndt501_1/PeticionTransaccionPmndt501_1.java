package com.bbva.pzic.peertopeer.dao.model.pmndt501_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PMNDT501</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPmndt501_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPmndt501_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PMNDT501-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PMNDT501&quot; application=&quot;PMND&quot; version=&quot;01&quot; country=&quot;PE&quot; language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;EntityIn&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.membership.quickresponse.dto.CreatePeerToPeerMembershipQuickResponseDTO&quot; artifactId=&quot;PMNDC501&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;customerId&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;8&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;membership-id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;8&quot;/&gt;
 * &lt;dto order=&quot;3&quot; name=&quot;alias&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.membership.quickresponse.dto.AliasDTO&quot; artifactId=&quot;PMNDC501&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;15&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;value&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;80&quot;/&gt;
 * &lt;dto order=&quot;3&quot; name=&quot;aliasType&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.membership.quickresponse.dto.AliasTypeDTO&quot; artifactId=&quot;PMNDC501&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;10&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;EntityOut&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.membership.quickresponse.dto.CreatePeerToPeerMembershipQuickResponseDTO&quot; artifactId=&quot;PMNDC501&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;dto order=&quot;2&quot; name=&quot;alias&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.membership.quickresponse.dto.AliasDTO&quot; artifactId=&quot;PMNDC501&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;15&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;value&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;80&quot;/&gt;
 * &lt;dto order=&quot;3&quot; name=&quot;aliasType&quot; mandatory=&quot;1&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.membership.quickresponse.dto.AliasTypeDTO&quot; artifactId=&quot;PMNDC501&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;10&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; mandatory=&quot;0&quot; type=&quot;String&quot; size=&quot;20&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;list order=&quot;3&quot; name=&quot;links&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto order=&quot;1&quot; name=&quot;link&quot; mandatory=&quot;0&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.membership.quickresponse.dto.LinkDTO&quot; artifactId=&quot;PMNDC501&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;href&quot; mandatory=&quot;1&quot; type=&quot;String&quot; size=&quot;80&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;creationDate&quot; mandatory=&quot;1&quot; type=&quot;Timestamp&quot; size=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;createPeerToPeerMembershipQuickResponse&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPmndt501_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PMNDT501",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPmndt501_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPmndt501_1 {
		
		/**
	 * <p>Campo <code>EntityIn</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "EntityIn", tipo = TipoCampo.DTO)
	private Entityin entityin;
	
}