package com.bbva.pzic.peertopeer.dao.model.pmndt202_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Transacci&oacute;n <code>PMNDT202</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPmndt202_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPmndt202_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PMNDT202-01-PE.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;PMNDT202&quot; application=&quot;PMND&quot; version=&quot;01&quot; country=&quot;PE&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;dto name=&quot;EntityIn&quot;
 * package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.ModifyPeerToPeerMembershipPaymentMethodDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;customerId&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;membership-id&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;payment-method-id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;alias&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.AliasDTO&quot; artifactId=&quot;PMNDC200&quot;
 * mandatory=&quot;0&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;value&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;dto name=&quot;aliasType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.AliasTypeDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * 
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;relatedContracts&quot; order=&quot;5&quot; mandatory=&quot;0&quot;&gt;
 * &lt;dto name=&quot;relatedContract&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.RelatedContractDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contractId&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;productType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.ProductTypeDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * 
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;relationType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.RelationTypeDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * 
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * 
 * &lt;parameter order=&quot;4&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;numberType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.NumberTypeDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;1&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;isAvailable&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;dto name=&quot;EntityOut&quot;
 * package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.ModifyPeerToPeerMembershipPaymentMethodDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;1&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;alias&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.AliasDTO&quot; artifactId=&quot;PMNDC200&quot;
 * mandatory=&quot;1&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;value&quot; type=&quot;String&quot; size=&quot;80&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;aliasType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.AliasTypeDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;list name=&quot;relatedContracts&quot; order=&quot;3&quot; mandatory=&quot;1&quot;&gt;
 * &lt;dto name=&quot;relatedContract&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.RelatedContractDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;0&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;contractId&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;productType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.ProductTypeDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;0&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;dto name=&quot;relationType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.RelationTypeDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;0&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/dto&gt;
 * 
 * &lt;parameter order=&quot;4&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;dto name=&quot;numberType&quot; package=&quot;com.bbva.pmnd.dto.peertopeer.paymentmethod.dto.NumberTypeDTO&quot;
 * artifactId=&quot;PMNDC200&quot; mandatory=&quot;1&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/dto&gt;
 * &lt;/list&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;isAvailable&quot; type=&quot;Boolean&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;lastUpdateDate&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;creationDate&quot; type=&quot;Timestamp&quot; size=&quot;0&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/dto&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;modifyPeerToPeerMembershipPaymentMethod&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionPmndt202_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PMNDT202",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionPmndt202_1.class,
	atributos = {@Atributo(nombre = "country", valor = "PE")}
)
@RooJavaBean
@RooSerializable
public class PeticionTransaccionPmndt202_1 {
		
		/**
	 * <p>Campo <code>EntityIn</code>, &iacute;ndice: <code>1</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 1, nombre = "EntityIn", tipo = TipoCampo.DTO)
	private Entityin entityin;
	
}