package com.bbva.pzic.peertopeer.dao.model.pmndt301_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PMNDT301</code>
 * 
 * @see PeticionTransaccionPmndt301_1
 * @see RespuestaTransaccionPmndt301_1
 */
@Component
public class TransaccionPmndt301_1 implements InvocadorTransaccion<PeticionTransaccionPmndt301_1,RespuestaTransaccionPmndt301_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPmndt301_1 invocar(PeticionTransaccionPmndt301_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt301_1.class, RespuestaTransaccionPmndt301_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPmndt301_1 invocarCache(PeticionTransaccionPmndt301_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt301_1.class, RespuestaTransaccionPmndt301_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}