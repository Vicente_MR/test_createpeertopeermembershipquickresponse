package com.bbva.pzic.peertopeer.dao.model.pmndt202_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>PMNDT202</code>
 * 
 * @see PeticionTransaccionPmndt202_1
 * @see RespuestaTransaccionPmndt202_1
 */
@Component
public class TransaccionPmndt202_1 implements InvocadorTransaccion<PeticionTransaccionPmndt202_1,RespuestaTransaccionPmndt202_1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionPmndt202_1 invocar(PeticionTransaccionPmndt202_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt202_1.class, RespuestaTransaccionPmndt202_1.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionPmndt202_1 invocarCache(PeticionTransaccionPmndt202_1 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionPmndt202_1.class, RespuestaTransaccionPmndt202_1.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}