package com.bbva.pzic.peertopeer.dao.model.pmndt202_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>EntityIn</code>, utilizado por la clase <code>PeticionTransaccionPmndt202_1</code></p>
 * 
 * @see PeticionTransaccionPmndt202_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooSerializable
public class Entityin {
	
	/**
	 * <p>Campo <code>customerId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "customerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true)
	private String customerid;
	
	/**
	 * <p>Campo <code>membership-id</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "membership-id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
	private String membershipId;
	
	/**
	 * <p>Campo <code>payment-method-id</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "payment-method-id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String paymentMethodId;
	
	/**
	 * <p>Campo <code>alias</code>, &iacute;ndice: <code>4</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 4, nombre = "alias", tipo = TipoCampo.DTO)
	private Alias alias;
	
	/**
	 * <p>Campo <code>relatedContracts</code>, &iacute;ndice: <code>5</code>, tipo: <code>LIST</code>
	 */
	@Campo(indice = 5, nombre = "relatedContracts", tipo = TipoCampo.LIST)
	private List<Relatedcontracts> relatedcontracts;
	
	/**
	 * <p>Campo <code>isAvailable</code>, &iacute;ndice: <code>6</code>, tipo: <code>BOOLEAN</code>
	 */
	@Campo(indice = 6, nombre = "isAvailable", tipo = TipoCampo.BOOLEAN, signo = true)
	private Boolean isavailable;
	
}