package com.bbva.pzic.peertopeer.dao.apx.mapper;

import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.PeticionTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.dao.model.pmndt202_1.RespuestaTransaccionPmndt202_1;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface IApxModifyPeerToPeerMembershipPaymentMethodMapper {
    PeticionTransaccionPmndt202_1 mapIn(InputModifyPeerToPeerMembershipPaymentMethod input);

    PaymentMethod mapOut(RespuestaTransaccionPmndt202_1 responseBackend);
}
