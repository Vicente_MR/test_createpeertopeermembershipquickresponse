package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class DTOIntPaymentMethod {

    @NotNull(groups = ValidationGroup.CreatePeerToPeerMembershipPaymentMethod.class)
    @Valid
    private DTOIntAlias alias;
    @NotNull(groups = ValidationGroup.CreatePeerToPeerMembershipPaymentMethod.class)
    @Valid
    private List<DTOIntRelatedContract> relatedContracts;

    private String id;

    private Boolean isAvailable;

    public DTOIntAlias getAlias() {
        return alias;
    }

    public void setAlias(DTOIntAlias alias) {
        this.alias = alias;
    }

    public List<DTOIntRelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<DTOIntRelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }
}
