package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class InputModifyPeerToPeerMembershipPaymentMethod {

    private String customerId;
    private String membershipId;
    private String paymentMethodId;
    @Valid
    @NotNull(groups = ValidationGroup.ModifyPeerToPeerMembershipPaymentMethod.class)
    private DTOIntPaymentMethod paymentMethod;

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public DTOIntPaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(DTOIntPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

}
