package com.bbva.pzic.peertopeer.business.dto;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class DTOIntPaymentMethodId {

    private String identifier;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
