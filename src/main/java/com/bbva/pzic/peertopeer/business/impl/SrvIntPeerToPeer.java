package com.bbva.pzic.peertopeer.business.impl;

import com.bbva.pzic.peertopeer.business.ISrvIntPeerToPeer;
import com.bbva.pzic.peertopeer.business.dto.*;
import com.bbva.pzic.peertopeer.dao.IPeerToPeerDAO;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Component
public class SrvIntPeerToPeer implements ISrvIntPeerToPeer {

    private static final Log LOG = LogFactory.getLog(SrvIntPeerToPeer.class);

    @Autowired
    private IPeerToPeerDAO peerToPeerDAO;
    @Autowired
    private Validator validator;

    /**
     * {@inheritDoc}
     */
    @Override
    public Membership getPeerToPeerMembership(
            final InputGetPeerToPeerMembership input) {
        LOG.info("... Invoking method SrvIntPeerToPeer.getPeerToPeerMembership ...");
        return peerToPeerDAO.getPeerToPeerMembership(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Membership modifyPeerToPeerMembership(
            final InputModifyPeerToPeerMembership input) {
        LOG.info("... Invoking method SrvIntPeerToPeer.modifyPeerToPeerMembership ...");
        LOG.info("... Validating modifyPeerToPeerMembership input parameter ...");
        validator.validate(input, ValidationGroup.ModifyPeerToPeerMembership.class);
        return peerToPeerDAO.modifyPeerToPeerMembership(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentMethod createPeerToPeerMembershipPaymentMethod(
            final InputCreatePeerToPeerMembershipPaymentMethod input) {
        LOG.info("... Invoking method SrvIntPeerToPeer.createPeerToPeerMembershipPaymentMethod ...");
        LOG.info("... Validating createPeerToPeerMembershipPaymentMethod input parameter ...");
        validator.validate(input, ValidationGroup.CreatePeerToPeerMembershipPaymentMethod.class);
        return peerToPeerDAO.createPeerToPeerMembershipPaymentMethod(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaymentMethod modifyPeerToPeerMembershipPaymentMethod(
            final InputModifyPeerToPeerMembershipPaymentMethod input) {
        LOG.info("... Invoking method SrvIntPeerToPeer.modifyPeerToPeerMembershipPaymentMethod ...");
        LOG.info("... Validating modifyPeerToPeerMembershipPaymentMethod input parameter ...");
        validator.validate(input, ValidationGroup.ModifyPeerToPeerMembershipPaymentMethod.class);
        return peerToPeerDAO.modifyPeerToPeerMembershipPaymentMethod(input);
    }

    @Override
    public ContactsBook searchPeerToPeerContactBook(final InputSearchPeerToPeerContactBook input) {
        LOG.info("... Invoking method SrvIntPeerToPeer.searchPeerToPeerContactBook ...");
        LOG.info("... Validating searchPeerToPeerContactBook input parameter ...");
        validator.validate(input, ValidationGroup.SearchPeerToPeerContactBook.class);
        return peerToPeerDAO.searchPeerToPeerContactBook(input);
    }

    @Override
    public QuickResponse createPeerToPeerMembershipQuickResponse(final InputCreatePeerToPeerMembershipQuickResponse input) {
        LOG.info("... Invoking method SrvIntPeerToPeer.createPeerToPeerMembershipQuickResponse ...");
        LOG.info("... Validating createPeerToPeerMembershipQuickResponse input parameter ...");
        validator.validate(input, ValidationGroup.CreatePeerToPeerMembershipQuickResponse.class);
        return peerToPeerDAO.createPeerToPeerMembershipQuickResponse(input);
    }
}
