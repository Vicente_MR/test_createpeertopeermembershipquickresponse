package com.bbva.pzic.peertopeer.business.dto;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class InputGetPeerToPeerMembership {

    private String customerId;
    private String membershipId;
    private String expand;

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getExpand() {
        return expand;
    }

    public void setExpand(String expand) {
        this.expand = expand;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
