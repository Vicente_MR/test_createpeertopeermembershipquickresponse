package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class DTOIntAlias {

    private String id;
    @NotNull(groups = {
            ValidationGroup.CreatePeerToPeerMembershipPaymentMethod.class,
            ValidationGroup.ModifyPeerToPeerMembershipPaymentMethod.class,
            ValidationGroup.CreatePeerToPeerMembershipQuickResponse.class})
    @Valid
    private DTOIntAliasType aliasType;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntAliasType getAliasType() {
        return aliasType;
    }

    public void setAliasType(DTOIntAliasType aliasType) {
        this.aliasType = aliasType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
