package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class DTOIntContactsBook {
    @Valid
    @NotNull(groups = ValidationGroup.SearchPeerToPeerContactBook.class)
    private List<DTOIntContact> contacts;

    public List<DTOIntContact> getContacts() {
        return contacts;
    }

    public void setContacts(List<DTOIntContact> contacts) {
        this.contacts = contacts;
    }
}
