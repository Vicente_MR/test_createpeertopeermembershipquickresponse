package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class InputCreatePeerToPeerMembershipQuickResponse {

    private String customerId;
    private String membershipId;

//    @Valid
//    @NotNull(groups = ValidationGroup.CreatePeerToPeerMembershipQuickResponse.class)
    private DTOIntAlias alias;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public DTOIntAlias getAlias() {
        return alias;
    }

    public void setAlias(DTOIntAlias alias) {
        this.alias = alias;
    }
}
