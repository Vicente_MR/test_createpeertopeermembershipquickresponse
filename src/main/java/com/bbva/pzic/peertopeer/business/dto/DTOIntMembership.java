package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import java.util.Date;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class DTOIntMembership {

    private String id;
    private Boolean isPreferedReceiver;
    @Valid
    private DTOIntStatus status;
    private Date membershipDate;
    private Date lastUpdateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsPreferedReceiver() {
        return isPreferedReceiver;
    }

    public void setIsPreferedReceiver(Boolean isPreferedReceiver) {
        this.isPreferedReceiver = isPreferedReceiver;
    }

    public DTOIntStatus getStatus() {
        return status;
    }

    public void setStatus(DTOIntStatus status) {
        this.status = status;
    }

    public Date getMembershipDate() {
        if (membershipDate == null) {
            return null;
        }
        return new Date(membershipDate.getTime());
    }

    public void setMembershipDate(Date membershipDate) {
        if (membershipDate == null) {
            this.membershipDate = null;
        } else {
            this.membershipDate = new Date(membershipDate.getTime());
        }
    }

    public Date getLastUpdateDate() {
        if (lastUpdateDate == null) {
            return null;
        }
        return new Date(lastUpdateDate.getTime());
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        if (lastUpdateDate == null) {
            this.lastUpdateDate = null;
        } else {
            this.lastUpdateDate = new Date(lastUpdateDate.getTime());
        }
    }
}
