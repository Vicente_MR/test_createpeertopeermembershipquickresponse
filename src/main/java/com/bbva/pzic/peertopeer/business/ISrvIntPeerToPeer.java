package com.bbva.pzic.peertopeer.business;

import com.bbva.pzic.peertopeer.business.dto.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.*;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface ISrvIntPeerToPeer {

    /**
     * It allows participants to obtain the information of a membership.
     *
     * @param input dto with input fields to validate
     * @return {@link Membership}
     */
    Membership getPeerToPeerMembership(InputGetPeerToPeerMembership input);

    /**
     * Allows you to update the membership.
     *
     * @param input dto with input fields to validate
     * @return {@link Membership}
     */
    Membership modifyPeerToPeerMembership(InputModifyPeerToPeerMembership input);

    /**
     * It allows the participants to add a payment method in order to make P2P
     * transfers.
     *
     * @param input dto with input fields to validate
     * @return {@link PaymentMethodId}
     */
    PaymentMethod createPeerToPeerMembershipPaymentMethod(InputCreatePeerToPeerMembershipPaymentMethod input);

    /**
     * Allows you to update the membership
     *
     * @param input dto with input fields to validate
     * @return {@link PaymentMethod}
     */
    PaymentMethod modifyPeerToPeerMembershipPaymentMethod(InputModifyPeerToPeerMembershipPaymentMethod input);

    ContactsBook searchPeerToPeerContactBook(InputSearchPeerToPeerContactBook inputSearchPeerToPeerContactBook);

    QuickResponse createPeerToPeerMembershipQuickResponse(InputCreatePeerToPeerMembershipQuickResponse input);
}
