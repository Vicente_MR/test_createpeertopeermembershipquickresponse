package com.bbva.pzic.peertopeer.business.dto;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class DTOIntNumberType {

    private String id;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
