package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class DTOIntRelationType {
    @NotNull(groups = {
            ValidationGroup.CreatePeerToPeerMembershipPaymentMethod.class,
            ValidationGroup.ModifyPeerToPeerMembershipPaymentMethod.class})
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
