package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class InputSearchPeerToPeerContactBook {
    private String customerId;
    @Valid
    @NotNull(groups = ValidationGroup.SearchPeerToPeerContactBook.class)
    private DTOIntContactsBook contactsBook;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public DTOIntContactsBook getContactsBook() {
        return contactsBook;
    }

    public void setContactsBook(DTOIntContactsBook contactsBook) {
        this.contactsBook = contactsBook;
    }
}
