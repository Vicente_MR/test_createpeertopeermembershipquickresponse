package com.bbva.pzic.peertopeer.business.dto;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface ValidationGroup {

    interface ModifyPeerToPeerMembership {
    }

    interface CreatePeerToPeerMembershipPaymentMethod {
    }

    interface ModifyPeerToPeerMembershipPaymentMethod {
    }

    interface SearchPeerToPeerContactBook {
    }

    interface CreatePeerToPeerMembershipQuickResponse {
    }
}
