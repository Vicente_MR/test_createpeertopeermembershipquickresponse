package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class DTOIntRelatedContracts {
    @NotNull(groups = {
            ValidationGroup.CreatePeerToPeerMembershipPaymentMethod.class,
            ValidationGroup.ModifyPeerToPeerMembershipPaymentMethod.class})
    private String contractId;
    @Valid
    private DTOIntProductType productType;
    @Valid
    private DTOIntRelationType relationType;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public DTOIntProductType getProductType() {
        return productType;
    }

    public void setProductType(DTOIntProductType productType) {
        this.productType = productType;
    }

    public DTOIntRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(DTOIntRelationType relationType) {
        this.relationType = relationType;
    }
}
