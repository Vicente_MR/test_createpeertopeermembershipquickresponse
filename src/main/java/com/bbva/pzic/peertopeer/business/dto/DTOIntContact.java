package com.bbva.pzic.peertopeer.business.dto;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DTOIntContact {

    @Valid
    private DTOIntContactType contactType;

    @Valid
    @NotNull(groups = ValidationGroup.SearchPeerToPeerContactBook.class)
    private String value;

    public DTOIntContactType getContactType() {
        return contactType;
    }

    public void setContactType(DTOIntContactType contactType) {
        this.contactType = contactType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
