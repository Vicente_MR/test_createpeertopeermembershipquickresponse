package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntContactType {
    @NotNull(groups = ValidationGroup.SearchPeerToPeerContactBook.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
