package com.bbva.pzic.peertopeer.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public class InputModifyPeerToPeerMembership {

    private String membershipId;

    private String customerId;

    @Valid
    @NotNull(groups = ValidationGroup.ModifyPeerToPeerMembership.class)
    private DTOIntMembership membership;

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public DTOIntMembership getMembership() {
        return membership;
    }

    public void setMembership(DTOIntMembership membership) {
        this.membership = membership;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
