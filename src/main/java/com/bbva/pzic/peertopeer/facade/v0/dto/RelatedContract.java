package com.bbva.pzic.peertopeer.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.servicing.annotations.SecurityFunction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "relatedContract", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "relatedContract", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RelatedContract implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier associated with the contract.
     */
    @DatoAuditable(omitir = true)
    @SecurityFunction(inFunction="decypher")
    private String contractId;
    /**
     * Financial product type associated to the contract.
     */
    private ProductType productType;
    /**
     * Type of relation between the related contract and the account.
     */
    private RelationType relationType;
    /**
     * Contract number.
     */
    @DatoAuditable(omitir = true)
    private String number;
    /**
     * Contract number type based on the financial product type.
     */
    private NumberType numberType;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public RelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }
}
