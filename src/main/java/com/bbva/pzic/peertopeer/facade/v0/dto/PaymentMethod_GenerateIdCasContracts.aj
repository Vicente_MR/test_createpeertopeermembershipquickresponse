package com.bbva.pzic.peertopeer.facade.v0.dto;

privileged aspect PaymentMethod_GenerateIdCasContracts {

    public java.util.Map<String, String> PaymentMethod.getIdCasContracts() {

        java.util.Map<String, String> idGenerates = new java.util.HashMap();

        try {
            for (int i = 0; i < getRelatedContracts().size(); i++) {
                RelatedContract relatedContract = getRelatedContracts().get(i);
                if (relatedContract == null || relatedContract.getContractId() == null) {
                    continue;
                }
                idGenerates.put(String.format("relatedContracts[%s].contractId", i),
                        relatedContract.getContractId());

                if (getAlias() != null && getAlias().getId() != null) {
                    idGenerates.put("alias.id", getAlias().getId());
                }
            }
        } catch (Exception relatedContracts_list_number11) {}
        return idGenerates;
    }

}
