package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.business.dto.*;
import com.bbva.pzic.peertopeer.facade.v0.dto.*;
import com.bbva.pzic.peertopeer.facade.v0.mapper.ICreatePeerToPeerMembershipPaymentMethodMapper;
import com.bbva.pzic.peertopeer.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class CreatePeerToPeerMembershipPaymentMethodMapper implements ICreatePeerToPeerMembershipPaymentMethodMapper {

    private static final Log LOG = LogFactory.getLog(CreatePeerToPeerMembershipPaymentMethodMapper.class);

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Override
    public InputCreatePeerToPeerMembershipPaymentMethod mapIn(final String membershipId, final PaymentMethod paymentMethod) {
        LOG.info("... called method CreatePeerToPeerMembershipPaymentMethodMapper.mapIn ...");
        InputCreatePeerToPeerMembershipPaymentMethod input = new InputCreatePeerToPeerMembershipPaymentMethod();
        input.setCustomerId(serviceInvocationContext.getProperty("ASTAMxClientId"));
        input.setMembershipId(membershipId);
        input.setPaymentMethod(getDtoIntPaymentMethod(paymentMethod));
        return input;
    }

    private DTOIntPaymentMethod getDtoIntPaymentMethod(final PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }
        DTOIntPaymentMethod dtoIntPaymentMethod = new DTOIntPaymentMethod();
        dtoIntPaymentMethod.setAlias(getIntAlias(paymentMethod.getAlias()));
        dtoIntPaymentMethod.setRelatedContracts(getRelatedcontractsList(paymentMethod.getRelatedContracts()));
        return dtoIntPaymentMethod;
    }

    private List<DTOIntRelatedContract> getRelatedcontractsList(List<RelatedContract> relatedContractList) {
        if (CollectionUtils.isEmpty(relatedContractList)) {
            return null;
        }
        return relatedContractList.stream().filter(Objects::nonNull).map(this::getDtoIntRelatedContract).collect(Collectors.toList());
    }

    private DTOIntRelatedContract getDtoIntRelatedContract(final RelatedContract relatedContract) {
        DTOIntRelatedContract dtoIntRelatedContract = new DTOIntRelatedContract();
        dtoIntRelatedContract.setContractId(relatedContract.getContractId());
        dtoIntRelatedContract.setProductType(getDtoIntProductType(relatedContract.getProductType()));
        dtoIntRelatedContract.setRelationType(getDtoIntRelationType(relatedContract.getRelationType()));
        return dtoIntRelatedContract;
    }

    private DTOIntProductType getDtoIntProductType(final ProductType productType) {
        if (productType == null) {
            return null;
        }
        DTOIntProductType dtoIntProductType = new DTOIntProductType();
        dtoIntProductType.setId(productType.getId());
        return dtoIntProductType;
    }

    private DTOIntRelationType getDtoIntRelationType(final RelationType relationType) {
        if (relationType == null) {
            return null;
        }
        DTOIntRelationType dtoIntRelationType = new DTOIntRelationType();
        dtoIntRelationType.setId(relationType.getId());
        return dtoIntRelationType;
    }

    private DTOIntAlias getIntAlias(final Alias alias) {
        if (alias == null) {
            return null;
        }
        DTOIntAlias dtoIntAlias = new DTOIntAlias();
        dtoIntAlias.setId(alias.getId());
        dtoIntAlias.setValue(alias.getValue());
        dtoIntAlias.setAliasType(getDtoIntAliasType(alias.getAliasType()));
        return dtoIntAlias;
    }

    private DTOIntAliasType getDtoIntAliasType(final AliasType aliasType) {
        if (aliasType == null) {
            return null;
        }
        DTOIntAliasType dtoIntAliasType = new DTOIntAliasType();
        dtoIntAliasType.setId(aliasType.getId());
        return dtoIntAliasType;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<PaymentMethod> mapOut(final PaymentMethod PaymentMethod) {
        LOG.info("... called method CreatePeerToPeerMembershipPaymentMethodMapper.mapOut ...");
        if (PaymentMethod == null) {
            return null;
        }
        return ServiceResponse.data(PaymentMethod).build();
    }
}
