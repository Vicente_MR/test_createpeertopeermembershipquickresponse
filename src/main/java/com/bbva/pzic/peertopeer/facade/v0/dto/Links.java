package com.bbva.pzic.peertopeer.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "links", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "links", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Links implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * URI of the target resource. The URI MUST be in relative form.
     */
    private String href;

    /**
     * Relationship between the containing entity and the entity linked to.
     */
    private String rel;

    /**
     * Link title you have to show (human readable).
     */
    private String title;

    /**
     * HTTP method that the client must use to consume the URI using this relation type.
     */
    private String method;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
