package com.bbva.pzic.peertopeer.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "contactsBook", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "contactsBook", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactsBook implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * User's mobile phone contact list.
     */
    private List<Contact> contacts;

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
