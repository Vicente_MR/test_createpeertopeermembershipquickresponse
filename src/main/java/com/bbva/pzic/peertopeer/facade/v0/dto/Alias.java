package com.bbva.pzic.peertopeer.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.servicing.annotations.SecurityFunction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "alias", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "alias", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Alias implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Alias unique identifier. This identifier belongs to the already
     * registered contact in the bank, such a preferred mobile number or email.
     */
    @DatoAuditable(omitir = true)
    @SecurityFunction(inFunction="decypher")
    private String id;
    /**
     * Type of alias. Possible values: email, phone or other form of how an
     * alias is defined for the participant and which will be used for the P2P
     * transfer.
     */
    private AliasType aliasType;
    /**
     * Alias value that informs the mobile number or email address. This
     * attribute is optional because the payment method can be registered with
     * de contact identifier already stored at the bank.
     */
    @DatoAuditable(omitir = true)
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AliasType getAliasType() {
        return aliasType;
    }

    public void setAliasType(AliasType aliasType) {
        this.aliasType = aliasType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
