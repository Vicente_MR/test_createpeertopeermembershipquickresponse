package com.bbva.pzic.peertopeer.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "status", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "status", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Status implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Membership status unique identifier.
     */
    private String id;
    /**
     * Membership status description.
     */
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
