package com.bbva.pzic.peertopeer.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.annotations.IdEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "paymentMethodId", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "paymentMethodId", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentMethodId implements Serializable {

    private static final long serialVersionUID = 1L;
    @IdEntity
    private String identifier;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
