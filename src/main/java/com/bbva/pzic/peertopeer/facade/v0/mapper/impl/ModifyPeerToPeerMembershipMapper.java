package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.business.dto.DTOIntMembership;
import com.bbva.pzic.peertopeer.business.dto.DTOIntStatus;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembership;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import com.bbva.pzic.peertopeer.facade.v0.dto.Status;
import com.bbva.pzic.peertopeer.facade.v0.mapper.IModifyPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyPeerToPeerMembershipMapper implements IModifyPeerToPeerMembershipMapper {

    private static final Log LOG = LogFactory.getLog(ModifyPeerToPeerMembershipMapper.class);

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Override
    public InputModifyPeerToPeerMembership mapIn(final String membershipId, final Membership membership) {
        LOG.info("... called method ModifyPeerToPeerMembershipMapper.mapIn ...");
        InputModifyPeerToPeerMembership input = new InputModifyPeerToPeerMembership();
        input.setCustomerId(serviceInvocationContext.getProperty("ASTAMxClientId"));
        input.setMembershipId(membershipId);
        input.setMembership(getDtoIntMembership(membership));
        return input;
    }

    private DTOIntMembership getDtoIntMembership(final Membership membership) {
        if (membership == null) {
            return null;
        }
        DTOIntMembership dtoIntMembership = new DTOIntMembership();
        dtoIntMembership.setIsPreferedReceiver(membership.getIsPreferedReceiver());
        dtoIntMembership.setStatus(getDtoIntStatus(membership.getStatus()));
        return dtoIntMembership;
    }

    private DTOIntStatus getDtoIntStatus(final Status status) {
        if (status == null) {
            return null;
        }
        DTOIntStatus dtoIntStatus = new DTOIntStatus();
        dtoIntStatus.setId(status.getId());
        return dtoIntStatus;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<Membership> mapOut(final Membership membership) {
        LOG.info("... called method ModifyPeerToPeerMembershipMapper.mapOut ...");
        if (membership == null) {
            return null;
        }
        return ServiceResponse.data(membership).build();
    }
}
