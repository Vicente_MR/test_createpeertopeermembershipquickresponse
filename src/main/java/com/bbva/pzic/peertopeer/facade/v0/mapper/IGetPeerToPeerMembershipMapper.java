package com.bbva.pzic.peertopeer.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.peertopeer.business.dto.InputGetPeerToPeerMembership;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface IGetPeerToPeerMembershipMapper {

    InputGetPeerToPeerMembership mapIn(String membershipId, String expand);

    ServiceResponse<Membership> mapOut(Membership membership);
}
