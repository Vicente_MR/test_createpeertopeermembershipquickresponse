package com.bbva.pzic.peertopeer.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipQuickResponse;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface ICreatePeerToPeerMembershipQuickResponseMapper {

    InputCreatePeerToPeerMembershipQuickResponse mapIn(String membershipId, QuickResponse quickResponse);

    ServiceResponse<QuickResponse> mapOut(QuickResponse quickResponse);

}
