package com.bbva.pzic.peertopeer.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembershipPaymentMethod;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface IModifyPeerToPeerMembershipPaymentMethodMapper {

    InputModifyPeerToPeerMembershipPaymentMethod mapIn(String membershipId, String paymentMethodId, PaymentMethod paymentMethod);

    ServiceResponse<PaymentMethod> mapOut(PaymentMethod paymentMethod);
}
