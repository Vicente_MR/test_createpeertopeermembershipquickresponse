package com.bbva.pzic.peertopeer.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "aliasType", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "aliasType", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AliasType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Alias type unique identifier.
     */
    @DatoAuditable(omitir = true)
    private String id;
    /**
     * Alias type description.
     */
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
