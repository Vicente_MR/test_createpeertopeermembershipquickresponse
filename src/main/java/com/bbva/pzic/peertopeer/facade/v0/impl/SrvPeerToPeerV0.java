package com.bbva.pzic.peertopeer.facade.v0.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.annotations.PATCH;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.pzic.peertopeer.business.ISrvIntPeerToPeer;
import com.bbva.pzic.peertopeer.facade.v0.ISrvPeerToPeerV0;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import com.bbva.pzic.peertopeer.facade.v0.dto.PaymentMethod;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import com.bbva.pzic.peertopeer.facade.v0.mapper.*;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.Map;

import static com.bbva.pzic.peertopeer.facade.RegistryIds.*;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Path("/v0")
@Produces(MediaType.APPLICATION_JSON)
@SN(registryID = "SNPE2000018", logicalID = "peer-to-peer")
@VN(vnn = "v0")
@Service
public class SrvPeerToPeerV0 implements ISrvPeerToPeerV0, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvPeerToPeerV0.class);

    private static final String MEMBERSHIP_ID = "membership-id";
    private static final String PAYMENT_METHOD_ID = "payment-method-id";

    public HttpHeaders httpHeaders;
    public UriInfo uriInfo;

    @Autowired
    private ISrvIntPeerToPeer srvIntPeerToPeer;

    @Autowired
    private IGetPeerToPeerMembershipMapper getPeerToPeerMembershipMapper;
    @Autowired
    private IModifyPeerToPeerMembershipMapper modifyPeerToPeerMembershipMapper;
    @Autowired
    private ICreatePeerToPeerMembershipPaymentMethodMapper createPeerToPeerMembershipPaymentMethodMapper;
    @Autowired
    private IModifyPeerToPeerMembershipPaymentMethodMapper modifyPeerToPeerMembershipPaymentMethodMapper;
    @Autowired
    private ISearchPeerToPeerContactBookMapper searchPeerToPeerContactBookMapper;
    @Autowired
    private ICreatePeerToPeerMembershipQuickResponseMapper createPeerToPeerMembershipQuickResponseMapper;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;
    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Override
    public void setUriInfo(UriInfo uriInfo) {
        this.uriInfo = uriInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/memberships/{membership-id}")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_PEER_TO_PEER_MEMBERSHIP, logicalID = "getPeerToPeerMembership", forcedCatalog = "asoCatalog")
    public ServiceResponse<Membership> getPeerToPeerMembership(
            @PathParam(MEMBERSHIP_ID) final String membershipId,
            @QueryParam("expand") final String expand) {
        LOG.info("----- Invoking service getPeerToPeerMembership -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(MEMBERSHIP_ID, membershipId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_PEER_TO_PEER_MEMBERSHIP, null, pathParams, null);

        ServiceResponse<Membership> serviceResponse = getPeerToPeerMembershipMapper.mapOut(
                srvIntPeerToPeer.getPeerToPeerMembership(
                        getPeerToPeerMembershipMapper.mapIn(pathParams.get(MEMBERSHIP_ID).toString(), expand)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_PEER_TO_PEER_MEMBERSHIP, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PATCH
    @Path("/memberships/{membership-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_PEER_TO_PEER_MEMBERSHIP, logicalID = "modifyPeerToPeerMembership", forcedCatalog = "asoCatalog")
    public ServiceResponse<Membership> modifyPeerToPeerMembership(
            @PathParam(MEMBERSHIP_ID) final String membershipId,
            final Membership membership) {
        LOG.info("----- Invoking service modifyPeerToPeerMembership -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(MEMBERSHIP_ID, membershipId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_PEER_TO_PEER_MEMBERSHIP, membership, pathParams, null);

        ServiceResponse<Membership> serviceResponse = modifyPeerToPeerMembershipMapper.mapOut(
                srvIntPeerToPeer.modifyPeerToPeerMembership(
                        modifyPeerToPeerMembershipMapper.mapIn(pathParams.get(MEMBERSHIP_ID).toString(), membership)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_PEER_TO_PEER_MEMBERSHIP, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/memberships/{membership-id}/payment-methods")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_PEER_TO_PEER_MEMBERSHIP_PAYMENT_METHOD, logicalID = "createPeerToPeerMembershipPaymentMethod", forcedCatalog = "asoCatalog")
    public ServiceResponse<PaymentMethod> createPeerToPeerMembershipPaymentMethod(
            @PathParam(MEMBERSHIP_ID) final String membershipId,
            final PaymentMethod paymentMethod) {
        LOG.info("----- Invoking service createPeerToPeerMembershipPaymentMethod -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(MEMBERSHIP_ID, membershipId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_PEER_TO_PEER_MEMBERSHIP_PAYMENT_METHOD, paymentMethod, pathParams, null);

        ServiceResponse<PaymentMethod> serviceResponse = createPeerToPeerMembershipPaymentMethodMapper.mapOut(
                srvIntPeerToPeer.createPeerToPeerMembershipPaymentMethod(
                        createPeerToPeerMembershipPaymentMethodMapper.mapIn(pathParams.get(MEMBERSHIP_ID).toString(), paymentMethod)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_PEER_TO_PEER_MEMBERSHIP_PAYMENT_METHOD, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PATCH
    @Path("/memberships/{membership-id}/payment-methods/{payment-method-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_PEER_TO_PEER_MEMBERSHIP_PAYMENT_METHOD, logicalID = "modifyPeerToPeerMembershipPaymentMethod", forcedCatalog = "asoCatalog")
    public ServiceResponse<PaymentMethod> modifyPeerToPeerMembershipPaymentMethod(
            @PathParam(MEMBERSHIP_ID) final String membershipId,
            @PathParam(PAYMENT_METHOD_ID) final String paymentMethodId,
            final PaymentMethod paymentMethod) {
        LOG.info("----- Invoking service modifyPeerToPeerMembershipPaymentMethod -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(MEMBERSHIP_ID, membershipId);
        pathParams.put(PAYMENT_METHOD_ID, paymentMethodId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_PEER_TO_PEER_MEMBERSHIP_PAYMENT_METHOD, paymentMethod, pathParams, null);

        ServiceResponse<PaymentMethod> serviceResponse = modifyPeerToPeerMembershipPaymentMethodMapper.mapOut(
                srvIntPeerToPeer.modifyPeerToPeerMembershipPaymentMethod(
                        modifyPeerToPeerMembershipPaymentMethodMapper.mapIn(
                                pathParams.get(MEMBERSHIP_ID).toString(), pathParams.get(PAYMENT_METHOD_ID).toString(), paymentMethod)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_PEER_TO_PEER_MEMBERSHIP_PAYMENT_METHOD, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @POST
    @Path("/contacts-book/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_SEARCH_PEER_TO_PEER_CONTACT_BOOK, logicalID = "searchPeerToPeerContactBook", forcedCatalog = "asoCatalog")
    public ServiceResponse<ContactsBook> searchPeerToPeerContactBook(final ContactsBook contactsBook) {
        LOG.info("----- Invoking service searchPeerToPeerContactBook -----");

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SEARCH_PEER_TO_PEER_CONTACT_BOOK, contactsBook, null, null);

        ServiceResponse<ContactsBook> serviceResponse = searchPeerToPeerContactBookMapper.mapOut(
                srvIntPeerToPeer.searchPeerToPeerContactBook(
                        searchPeerToPeerContactBookMapper.mapIn(contactsBook)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SEARCH_PEER_TO_PEER_CONTACT_BOOK, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @POST
    @Path("/memberships/{membership-id}/quick-responses")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_PEER_TO_PEER_MEMBERSHIP_QUICK_RESPONSE, logicalID = "createPeerToPeerMembershipQuickResponse", forcedCatalog = "gabiCatalog")
    public ServiceResponse<QuickResponse> createPeerToPeerMembershipQuickResponse(
            @PathParam(MEMBERSHIP_ID) final String membershipId,
            QuickResponse quickResponse) {
        LOG.info("----- Invoking service createPeerToPeerMembershipQuickResponse -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(MEMBERSHIP_ID, membershipId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_PEER_TO_PEER_MEMBERSHIP_QUICK_RESPONSE, quickResponse, pathParams, null);
        ServiceResponse<QuickResponse> serviceResponse = createPeerToPeerMembershipQuickResponseMapper.mapOut(
                srvIntPeerToPeer.createPeerToPeerMembershipQuickResponse(createPeerToPeerMembershipQuickResponseMapper.mapIn(pathParams.get(MEMBERSHIP_ID).toString(), quickResponse)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_PEER_TO_PEER_MEMBERSHIP_QUICK_RESPONSE, serviceResponse, null, null);
        return null;
    }

}
