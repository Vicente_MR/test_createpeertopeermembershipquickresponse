package com.bbva.pzic.peertopeer.facade.v0.dto;

import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "membership", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "membership", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Membership implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Membership unique identifier.
     */
    private String id;
    /**
     * Informs if the BBVA payment method is actually chosen like the prefered
     * one, or the prefered payment method belongs to any other bank.
     */
    private Boolean isPreferedReceiver;
    /**
     * Name of the participant.
     */
    private Status status;
    /**
     * String based on ISO-8601 to specify the date the membership was
     * performed.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar membershipDate;
    /**
     * String based on ISO-8601 for specifying the last date when the membership
     * was updated.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar lastUpdateDate;

    private List<PaymentMethod> paymentMethods;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsPreferedReceiver() {
        return isPreferedReceiver;
    }

    public void setIsPreferedReceiver(Boolean isPreferedReceiver) {
        this.isPreferedReceiver = isPreferedReceiver;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Calendar getMembershipDate() {
        return membershipDate;
    }

    public void setMembershipDate(Calendar membershipDate) {
        this.membershipDate = membershipDate;
    }

    public Calendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Calendar lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Boolean getPreferedReceiver() {
        return isPreferedReceiver;
    }

    public void setPreferedReceiver(Boolean preferedReceiver) {
        isPreferedReceiver = preferedReceiver;
    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
}
