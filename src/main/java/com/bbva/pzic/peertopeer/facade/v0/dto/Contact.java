package com.bbva.pzic.peertopeer.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "contact", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "contact", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact type associated to the customer.
     */
    private ContactType contactType;
    /**
     * Contact value related to the contact type.
     */
    @DatoAuditable(omitir = true)
    private String value;
    /**
     * Name of the contact.
     */
    @DatoAuditable(omitir = true)
    private String firstName;
    /**
     * Last name of the contact.
     */
    @DatoAuditable(omitir = true)
    private String lastName;
    /**
     * Informs if the contact is affiliated to P2P transfers.
     */
    private Boolean isActive;

    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
