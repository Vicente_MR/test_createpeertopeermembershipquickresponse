package com.bbva.pzic.peertopeer.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.peertopeer.business.dto.InputModifyPeerToPeerMembership;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface IModifyPeerToPeerMembershipMapper {

    InputModifyPeerToPeerMembership mapIn(String membershipId, Membership membership);

    ServiceResponse<Membership> mapOut(Membership membership);
}
