package com.bbva.pzic.peertopeer.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.peertopeer.facade.v0.dto.*;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
public interface ISrvPeerToPeerV0 {

    /**
     * It allows participants to obtain the information of a membership.
     *
     * @param membershipId unique membership identifier
     * @param expand       expand the informed resource
     * @return {@link ServiceResponse<Membership>}
     */
    ServiceResponse<Membership> getPeerToPeerMembership(String membershipId, String expand);

    /**
     * Allows you to update the membership.
     *
     * @param membershipId unique membership identifier
     * @param membership   payload
     * @return {@link ServiceResponse<Membership>}
     */
    ServiceResponse<Membership> modifyPeerToPeerMembership(String membershipId,
                                                           Membership membership);

    /**
     * It allows the participants to add a payment method in order to make P2P
     * transfers.
     *
     * @param membershipId  unique membership identifier
     * @param paymentMethod payload
     * @return {@link ServiceResponse<PaymentMethodId>}
     */
    ServiceResponse<PaymentMethod> createPeerToPeerMembershipPaymentMethod(String membershipId,
                                                                           PaymentMethod paymentMethod);

    /**
     * Allows you to update the membership
     *
     * @param membershipId    unique membership identifier
     * @param paymentMethodId unique payment method identifier
     * @param paymentMethod   payload
     * @return {@link ServiceResponse<PaymentMethod>}
     */
    ServiceResponse<PaymentMethod> modifyPeerToPeerMembershipPaymentMethod(String membershipId,
                                                                           String paymentMethodId,
                                                                           PaymentMethod paymentMethod);

    ServiceResponse<ContactsBook> searchPeerToPeerContactBook(ContactsBook contactsBook);

    /**
     * It allows to create a quick response associated with the customer membership in order to use P2P services.
     *
     * @param membershipId unique membership identifier
     */
    ServiceResponse<QuickResponse> createPeerToPeerMembershipQuickResponse(String membershipId,
                                                                           QuickResponse quickResponse);

}
