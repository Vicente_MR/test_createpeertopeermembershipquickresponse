package com.bbva.pzic.peertopeer.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.peertopeer.business.dto.InputSearchPeerToPeerContactBook;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;

/**
 * Created on 2/26/2020.
 *
 * @author Cesardl
 */
public interface ISearchPeerToPeerContactBookMapper {
    InputSearchPeerToPeerContactBook mapIn(ContactsBook contactsBook);

    ServiceResponse<ContactsBook> mapOut(ContactsBook contactsBook);
}
