package com.bbva.pzic.peertopeer.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "contactType", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "contactType", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact type identifier. Possible values: email, phone or other form of how contact
     * is defined for the participant and which will be used for the P2P transfer.
     */
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
