package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.business.dto.InputGetPeerToPeerMembership;
import com.bbva.pzic.peertopeer.facade.v0.dto.Membership;
import com.bbva.pzic.peertopeer.facade.v0.mapper.IGetPeerToPeerMembershipMapper;
import com.bbva.pzic.peertopeer.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 26/02/2020.
 *
 * @author Entelgy
 */
@Mapper
public class GetPeerToPeerMembershipMapper implements IGetPeerToPeerMembershipMapper {

    private static final Log LOG = LogFactory.getLog(GetPeerToPeerMembershipMapper.class);

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Override
    public InputGetPeerToPeerMembership mapIn(final String membershipId, final String expand) {
        LOG.info("... called method GetPeerToPeerMembershipMapper.mapIn ...");
        InputGetPeerToPeerMembership input = new InputGetPeerToPeerMembership();
        input.setMembershipId(membershipId);
        input.setCustomerId(serviceInvocationContext.getProperty("ASTAMxClientId"));
        input.setExpand(expand);
        return input;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<Membership> mapOut(final Membership membership) {
        LOG.info("... called method GetPeerToPeerMembershipMapper.mapOut ...");
        if (membership == null) {
            return null;
        }
        return ServiceResponse.data(membership).build();
    }
}
