package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.business.dto.DTOIntContact;
import com.bbva.pzic.peertopeer.business.dto.DTOIntContactType;
import com.bbva.pzic.peertopeer.business.dto.DTOIntContactsBook;
import com.bbva.pzic.peertopeer.business.dto.InputSearchPeerToPeerContactBook;
import com.bbva.pzic.peertopeer.facade.v0.dto.Contact;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactType;
import com.bbva.pzic.peertopeer.facade.v0.dto.ContactsBook;
import com.bbva.pzic.peertopeer.facade.v0.mapper.ISearchPeerToPeerContactBookMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 2/26/2020.
 *
 * @author Cesardl
 */
@Component
public class SearchPeerToPeerContactBookMapper implements ISearchPeerToPeerContactBookMapper {

    private static final Log LOG = LogFactory.getLog(SearchPeerToPeerContactBookMapper.class);

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Override
    public InputSearchPeerToPeerContactBook mapIn(final ContactsBook contactsBook) {
        LOG.info("... called method SearchPeerToPeerContactBookMapper.mapIn ...");
        InputSearchPeerToPeerContactBook searchPeerToPeerContactBook = new InputSearchPeerToPeerContactBook();
        searchPeerToPeerContactBook.setCustomerId(serviceInvocationContext.getProperty("ASTAMxClientId"));
        searchPeerToPeerContactBook.setContactsBook(getDtoIntContactsBook(contactsBook));
        return searchPeerToPeerContactBook;
    }

    private DTOIntContactsBook getDtoIntContactsBook(final ContactsBook contactsBook) {
        if (contactsBook == null) {
            return null;
        }
        DTOIntContactsBook dtoIntContactsBook = new DTOIntContactsBook();
        dtoIntContactsBook.setContacts(getDtoIntContactList(contactsBook.getContacts()));
        return dtoIntContactsBook;
    }

    private List<DTOIntContact> getDtoIntContactList(final List<Contact> contacts) {
        if (CollectionUtils.isEmpty(contacts)) {
            return null;
        }
        return contacts.stream().filter(Objects::nonNull).map(this::getDtoIntContact).collect(Collectors.toList());
    }

    private DTOIntContact getDtoIntContact(final Contact contact) {

        DTOIntContact dtoIntContact = new DTOIntContact();
        dtoIntContact.setValue(contact.getValue());
        dtoIntContact.setContactType(getDtoIntContactType(contact.getContactType()));
        return dtoIntContact;
    }

    private DTOIntContactType getDtoIntContactType(final ContactType contactType) {
        if (contactType == null) {
            return null;
        }
        DTOIntContactType dtoIntContactType = new DTOIntContactType();
        dtoIntContactType.setId(contactType.getId());
        return dtoIntContactType;
    }

    @Override
    public ServiceResponse<ContactsBook> mapOut(final ContactsBook contactsBook) {
        LOG.info("... called method SearchPeerToPeerContactBookMapper.mapOut ...");
        if (contactsBook == null) {
            return null;
        }
        return ServiceResponse.data(contactsBook).build();
    }
}
