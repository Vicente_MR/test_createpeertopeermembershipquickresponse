package com.bbva.pzic.peertopeer.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.peertopeer.business.dto.DTOIntAlias;
import com.bbva.pzic.peertopeer.business.dto.DTOIntAliasType;
import com.bbva.pzic.peertopeer.business.dto.InputCreatePeerToPeerMembershipQuickResponse;
import com.bbva.pzic.peertopeer.facade.v0.dto.Alias;
import com.bbva.pzic.peertopeer.facade.v0.dto.AliasType;
import com.bbva.pzic.peertopeer.facade.v0.dto.QuickResponse;
import com.bbva.pzic.peertopeer.facade.v0.mapper.ICreatePeerToPeerMembershipQuickResponseMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreatePeerToPeerMembershipQuickResponseMapper implements ICreatePeerToPeerMembershipQuickResponseMapper {

    private static final Log LOG = LogFactory.getLog(CreatePeerToPeerMembershipQuickResponseMapper.class);

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Override
    public InputCreatePeerToPeerMembershipQuickResponse mapIn(final String membershipId, final QuickResponse quickResponse) {
        LOG.info("... called method CreatePeerToPeerMembershipQuickResponseMapper.mapIn ...");
        InputCreatePeerToPeerMembershipQuickResponse input = new InputCreatePeerToPeerMembershipQuickResponse();
        input.setCustomerId(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID));
        input.setMembershipId(membershipId);
        input.setAlias(mapInAlias(quickResponse.getAlias()));
        return input;
    }

    private DTOIntAlias mapInAlias(final Alias alias) {
        if(alias == null){
            return null;
        }

        DTOIntAlias dtoInt = new DTOIntAlias();
        dtoInt.setId(alias.getId());
        dtoInt.setAliasType(mapInAliasType(alias.getAliasType()));
        dtoInt.setValue(alias.getValue());

        return dtoInt;
    }

    private DTOIntAliasType mapInAliasType(final AliasType aliasType) {
        if(aliasType == null){
            return null;
        }

        DTOIntAliasType dtoInt = new DTOIntAliasType();
        dtoInt.setId(aliasType.getId());
        return dtoInt;
    }

    @Override
    public ServiceResponse<QuickResponse> mapOut(final QuickResponse quickResponse) {
        LOG.info("... called method CreatePeerToPeerMembershipQuickResponseMapper.mapOut ...");

        if(quickResponse == null){
            return null;
        }

        return ServiceResponse.data(quickResponse).build();
    }
}
