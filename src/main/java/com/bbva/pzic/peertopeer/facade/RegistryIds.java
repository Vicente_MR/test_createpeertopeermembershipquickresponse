package com.bbva.pzic.peertopeer.facade;

/**
 * @author Entelgy
 */
public final class RegistryIds {

    public static final String SMC_REGISTRY_ID_OF_GET_PEER_TO_PEER_MEMBERSHIP = "SMCPE2010145";
    public static final String SMC_REGISTRY_ID_OF_MODIFY_PEER_TO_PEER_MEMBERSHIP = "SMCPE2010146";
    public static final String SMC_REGISTRY_ID_OF_CREATE_PEER_TO_PEER_MEMBERSHIP_PAYMENT_METHOD = "SMCPE2010147";
    public static final String SMC_REGISTRY_ID_OF_MODIFY_PEER_TO_PEER_MEMBERSHIP_PAYMENT_METHOD = "SMCPE2010149";
    public static final String SMC_REGISTRY_ID_OF_SEARCH_PEER_TO_PEER_CONTACT_BOOK = "SMCPE2010150";
    public static final String SMC_REGISTRY_ID_OF_CREATE_PEER_TO_PEER_MEMBERSHIP_QUICK_RESPONSE = "SMGG20204037";

    private RegistryIds() {
    }
}
