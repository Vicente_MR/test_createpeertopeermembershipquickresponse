package com.bbva.pzic.peertopeer.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.annotations.IdEntity;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "paymentMethod", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlType(name = "paymentMethod", namespace = "urn:com:bbva:pzic:peertopeer:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentMethod implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Alias with which the customer identifies.
     */

    private Alias alias;
    /**
     * Related contracts of the account. An account can have several related
     * contracts, each one associated with a different type of relationship.
     */
    private List<RelatedContract> relatedContracts;
    /**
     * Membership unique identifier.
     */
    @IdEntity
    private String id;
    /**
     * Informs if the payment method full fills all the conditions in order to
     * be used to perform any operation.
     */
    private Boolean isAvailable;

    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar creationDate;
    /**
     * String based on ISO-8601 for specifying the last date when the membership
     * was updated.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name = "dateTime")
    private Calendar lastUpdateDate;

    public Alias getAlias() {
        return alias;
    }

    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    public List<RelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<RelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public Calendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Calendar lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
}
